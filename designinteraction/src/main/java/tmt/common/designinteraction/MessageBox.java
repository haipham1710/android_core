package tmt.common.designinteraction;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

import com.afollestad.materialdialogs.MaterialDialog;

public class MessageBox {
    public static void show(FragmentManager fm, String content) {
        show(fm, content, null);
    }

    public static void show(FragmentManager fm, String content, String title) {
        AlertDialogFragment<Fragment> dialog = new AlertDialogFragment<>();
        dialog.setContent(content);
        dialog.setTitle(title);
        dialog.setPositive("OK");
        dialog.show(fm, null);
    }


    @Deprecated
    public static Dialog progress(Context context, String content) {
        return progress(context, content, null);
    }

    @Deprecated
    public static Dialog progress(Context context, String content, String title) {
        return new MaterialDialog.Builder(context)
                .content(content)
                .title(title)
                .cancelable(false)
                .progress(true, 0)
                .show();
    }

    public static void showThenFinish(Fragment target, String message) {
        new FinishActivityDialog().show(target, message);
    }

    public static void showThenFinish(FragmentActivity activity, String message) {
        new FinishActivityDialog().show(activity, message);
    }

    public static void showThenFinish(FragmentActivity activity, String message, String title) {

        new FinishActivityDialog().show(activity, message, title);
    }

    public static class FinishActivityDialog extends AlertDialogFragment<Fragment> {

        public void show(Fragment target, String content) {
            setPositive("OK");
            setContent(content);
            super.show(target);
        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                activity.finish();
            }
        }

        private void show(FragmentActivity activity, String content) {
            setContent(content);
            setPositive("OK");
            super.show(activity);
        }

        private void show(FragmentActivity activity, String content, String title) {
            setTitle(title);
            show(activity, content);
        }
    }
}
