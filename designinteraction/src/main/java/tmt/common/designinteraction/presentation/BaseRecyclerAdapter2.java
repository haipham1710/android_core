package tmt.common.designinteraction.presentation;

import android.content.Context;
import android.os.Parcelable;

public abstract class BaseRecyclerAdapter2<T extends Parcelable> extends BaseRecyclerAdapter<T> {
    public enum LoadMode {
        None, Bottom, Top
    }

    private LoadMode loadMode = LoadMode.None;
    protected int itemRemainingToLoad = 5;

    public BaseRecyclerAdapter2() {

    }

    public BaseRecyclerAdapter2(Context context) {
        super(context);
    }

    public BaseRecyclerAdapter2(Context context, Iterable<T> items) {
        super(context, items);
    }

    public BaseRecyclerAdapter2(Context context, T empty, T[] items) {
        super(context, empty, items);
    }

    public BaseRecyclerAdapter2(Context context, T[] items) {
        super(context, items);
    }

    public BaseRecyclerAdapter2(Context context, T empty, Iterable<T> items) {
        super(context, empty, items);
    }

    protected void setLoadMode(LoadMode loadMode) {
        this.loadMode = loadMode;
    }

    @Override
    public final void onBindViewHolder(BindableViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);
        if (loadMode == LoadMode.Bottom) {
            if (position == size() - itemRemainingToLoad) {
                fetch();
            }
        } else if (loadMode == LoadMode.Top) {
            if (position == itemRemainingToLoad) {
                fetch();
            }
        }
    }
}
