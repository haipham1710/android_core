package tmt.common.designinteraction.presentation;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import tmt.common.util.SavedInstanceStateData;

public abstract class HeaderRecyclerAdapter extends FetchableRecyclerAdapter {

    public BaseRecyclerAdapter<?> getBaseAdapter() {
        return base;
    }

    static final int HEADER_VIEW_TYPE = 1321;
    static final int HEADER_VIEW_EMPTY = 1323;
    private final BaseRecyclerAdapter<?> base;

    public HeaderRecyclerAdapter(BaseRecyclerAdapter<?> base) {
        this.base = base;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return HEADER_VIEW_TYPE;
        } else if (base.isEnableEmptyRow() && base.isEmpty()) {
            return HEADER_VIEW_EMPTY;
        }
        return base.getItemViewType(position - 1);
    }

    @Override
    public BindableViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == HEADER_VIEW_TYPE) {
            return new HeaderViewHolder(inflateHeaderView(base.inflater(), parent));
        }
        if (viewType == HEADER_VIEW_EMPTY) {
            return new EmptyViewHolder(makeEmptyViewOfDefault(base.inflater(), parent));
        }
        return base.onCreateViewHolder(parent, viewType);
    }

    private View makeEmptyViewOfDefault(LayoutInflater inflater, ViewGroup parent) {
        View emptyView = inflateEmptyView(inflater, parent);
        if (emptyView != null) {
            return emptyView;
        }
        TextView textView = new TextView(inflater.getContext());
        textView.setId(android.R.id.empty);
        return textView;
    }

    public View inflateEmptyView(LayoutInflater inflater, ViewGroup parent) {
        return null;
    }

    public abstract View inflateHeaderView(LayoutInflater inflater, ViewGroup parent);


    @Override
    public void onBindViewHolder(BindableViewHolder holder, int position) {
        if (holder instanceof EmptyViewHolder) {
            ((EmptyViewHolder) holder).bind(base.getEmptyText());
        } else if (holder instanceof HeaderViewHolder) {
        } else {
            base.onBindViewHolder(holder, position - 1);
        }
    }

    @Override
    public void setContext(Context context) {
        base.setContext(context);
    }

    @Override
    public int getItemCount() {
        if (base.isEnableEmptyRow() && base.isEmpty()) {
            return 2;
        }
        return base.getItemCount() + 1;
    }

    @Override
    public void fetch() {
        base.fetch();
    }

    @Override
    public void setFetchingListener(FetchingListener listener) {
        base.setFetchingListener(listener);
    }

    @Override
    public boolean isFetching() {
        return base.isFetching();
    }

    @Override
    public boolean refresh() {
        return base.refresh();
    }

    @Override
    public void reset() {
        base.reset();
    }

    @Override
    public CharSequence getEmptyText() {
        return base.getEmptyText();
    }

    @Override
    public boolean isEmpty() {
        return base.isEmpty();
    }

    @Override
    public void reset(String keyword) {
        base.reset(keyword);
    }

    public Context getContext() {
        return base.getContext();
    }

    @Override
    public void registerAdapterDataObserver(RecyclerView.AdapterDataObserver observer) {
        base.registerAdapterDataObserver(observer);
    }

    @Override
    public void unregisterAdapterDataObserver(RecyclerView.AdapterDataObserver observer) {
        base.unregisterAdapterDataObserver(observer);
    }

    @NonNull
    @Override
    public SavedInstanceStateData saveState() {
        return base.saveState();
    }

    @Override
    public void restoreState(SavedInstanceStateData savedState) {
        base.restoreState(savedState);
    }

    static final class HeaderViewHolder extends BaseRecyclerAdapter.BindableViewHolder {

        public HeaderViewHolder(View itemView) {
            super(itemView);
            setIsRecyclable(false);
        }
    }

    static final class EmptyViewHolder extends BaseRecyclerAdapter.BindableViewHolder {
        final TextView textView;

        public EmptyViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(android.R.id.empty);
            setIsRecyclable(false);
        }

        public void bind(CharSequence emptyText) {
            textView.setText(emptyText);
        }

    }

}
