package tmt.common.designinteraction.presentation;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import tmt.common.presentation.FetchableAdapter;
import tmt.common.util.SavedInstanceStateData;

public abstract class FetchableRecyclerAdapter extends RecyclerView.Adapter<FetchableRecyclerAdapter.BindableViewHolder>
        implements FetchableAdapter {

    @NonNull
    public abstract SavedInstanceStateData saveState();

    public abstract void restoreState(SavedInstanceStateData savedState);

    public abstract CharSequence getEmptyText();


    public static class BindableViewHolder extends RecyclerView.ViewHolder {
        public BindableViewHolder(View itemView) {
            super(itemView);
        }

        public void bind(int position) {

        }

        public void setItem(Object item) {
        }
    }

    /**
     * Bridging from {@code tmt.common.presentation.ViewHolder} to {@code RecyclerView.ViewHolder}
     *
     * @param <T>
     */
    public static class TypedViewHolder<T> extends BindableViewHolder {
        private T item;
        protected final View view;

        public TypedViewHolder(View itemView) {
            super(itemView);
            view = itemView;
        }

        public T item() {
            return item;
        }

        @Override
        public void setItem(Object value) {
            item = (T) value;
        }

        @Override
        public void bind(int position) {

        }
    }
}
