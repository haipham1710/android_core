package tmt.common.designinteraction.presentation;

import android.content.Context;
import android.os.Parcelable;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.net.SocketException;
import java.net.UnknownHostException;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import tmt.common.ArrayHelper;
import tmt.common.presentation.Activator;
import tmt.common.presentation.DefaultConstructor;
import tmt.common.presentation.FetchableAdapter;
import tmt.common.presentation.Sortable;
import tmt.common.threading.Task;
import tmt.common.threading.TaskCompletion;
import tmt.common.threading.TaskSource;
import tmt.common.util.SavedInstanceStateData;

public abstract class BaseRecyclerAdapter<CollectionType> extends FetchableRecyclerAdapter
        implements Iterable<CollectionType> {

    private boolean canNext = true;
    private boolean fetching = false;
    private int page = 0;
    private boolean isEnableEmptyRow;
    private final ArrayList<CollectionType> items = new ArrayList<>();
    private LayoutInflater layoutInflater;
    private Context context;
    private FetchableAdapter.FetchingListener fetchingListener;
    private CharSequence emptyText;
    private CharSequence loadingText;
    private boolean pendingCleared;
    private Exception currentException;

    public BaseRecyclerAdapter() {

    }

    public BaseRecyclerAdapter(Context context) {
        this(context, null, (Iterable<CollectionType>) null);
    }

    public BaseRecyclerAdapter(Context context, CollectionType empty, CollectionType[] items) {
        this(context, empty, ArrayHelper.toIterable(items));
    }

    public BaseRecyclerAdapter(Context context, CollectionType[] items) {
        this(context, ArrayHelper.toIterable(items));
    }

    public BaseRecyclerAdapter(Context context, Iterable<CollectionType> items) {
        this(context, null, items);
    }

    public BaseRecyclerAdapter(Context context, CollectionType empty, Iterable<CollectionType> items) {
        setContext(context);
        if (empty != null) {
            this.items.add(empty);
        }
        if (items != null) {
            pendingCleared = true;
            for (CollectionType item : items) {
                this.items.add(item);
            }
        }
    }

    public void setContext(Context context) {
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
    }

    public void add(CollectionType item) {
        if (item == null) {
            throw new InvalidParameterException("item is added to collection cannot be null");
        }
        items.add(item);
    }

    public void clear() {
        canNext = true;
        page = 0;
        items.clear();
    }

    public boolean isEnableEmptyRow() {
        return isEnableEmptyRow;
    }

    @Override
    public void fetch() {
        if (!canNext || fetching) {
            return;
        }
        fetching = true;
        if (fetchingListener != null) {
            fetchingListener.onBeginFetch();
        }
        realizeItems(++page).then(new TaskCompletion<List<CollectionType>>() {

            @Override
            public void onContinue(Task<List<CollectionType>> task) {
                boolean shouldNotify = fetching;
                boolean hasValue = false;
                fetching = false;

                if (task.isFaulted()) {
                    currentException = task.getException();
                    onHasException(currentException);
                } else if (task.isCompleted()) {
                    if (pendingCleared) {
                        clear();
                        page = 1;
                    }
                    hasValue = task.getResult() != null && !task.getResult().isEmpty();

                    if (hasValue) {
                        addAll(task.getResult());
                    }
                }
                canNext = canNext && hasValue;
                pendingCleared = false;
                if (shouldNotify) {
                    notifyDataSetChanged();
                    onCompleteFetch();
                }
            }
        });
    }

    @NonNull
    protected Task<List<CollectionType>> realizeItems(int page) {
        return TaskSource.cancelledTask();
    }

    public CollectionType findItem(CollectionType item) {
        for (CollectionType element : items) {
            if (element.equals(item)) {
                return element;
            }
        }
        return null;
    }

    protected final Context getContext() {
        return context;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public abstract BindableViewHolder onCreateViewHolder(ViewGroup parent, int viewType);

    @Override
    public void onBindViewHolder(BindableViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.bind(position);
    }

    public CollectionType getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return (long) position;
    }

    public int getItemIndex(CollectionType item) {
        int i = 0;
        for (CollectionType e : items) {
            if (e.equals(item)) {
                return i;
            }
            i += 1;
        }
        return -1;
    }

    public final void addAll(CollectionType[] data) {
        for (CollectionType e : data) {
            add(e);
        }
    }

    protected void onCompleteFetch() {
        if (fetchingListener != null) {
            fetchingListener.onCompleteFetch();
        }
    }

    protected void onHasException(Exception ex) {
        if (fetchingListener != null) {
            fetchingListener.hasException(ex);
        }
    }

    public final void addAll(Iterable<CollectionType> data) {
        for (CollectionType e : data) {
            add(e);
        }
    }

    protected final LayoutInflater inflater() {
        return layoutInflater;
    }

    public final CollectionType remove(int position) {
        return items.remove(position);
    }

    public final boolean remove(CollectionType item) {
        return items.remove(item);
    }

    public <T extends Comparable<T>> void sortDescending(Sortable<CollectionType, T> x) {
        Collections.sort(items, Sortable.Helper.toComparator(x));
    }

    @Override
    public void reset() {
        clear();
        notifyDataSetChanged();
        fetch();
    }

    @Override
    public void reset(String keyword) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean refresh() {
        if (fetching) {
            return false;
        }
        canNext = true;
        page = 0;
        pendingCleared = true;
        fetch();
        return true;
    }

    protected static String getEmptyTextByException(Exception exception) {
        if (exception == null) {
            return null;
        }
        if (exception instanceof SocketException || exception instanceof UnknownHostException) {
            return "Mất kết nối";
        }
        return exception.getMessage();
    }

    public String getErrorMessage() {
        return getEmptyTextByException(currentException);
    }

    public boolean isEmpty() {
        return items.size() == 0;
    }

    @Override
    public void setFetchingListener(FetchingListener listener) {
        fetchingListener = listener;
    }

    protected final ArrayList<CollectionType> getItems() {
        return items;
    }

    public void insertFirst(CollectionType item) {
        items.add(0, item);
    }

    public final void add(int position, CollectionType item) {
        items.add(position, item);
    }

    protected void insertAfter(CollectionType item, CollectionType newItem) {
        items.add(items.indexOf(item) + 1, newItem);
    }

    protected void enableEmptyRow(CharSequence loadingText, CharSequence emptyText) {
        this.isEnableEmptyRow = true;
        this.emptyText = emptyText;
        this.loadingText = loadingText;
    }

    @Override
    public boolean isFetching() {
        return fetching;
    }

    public boolean hasKeyword() {
        return false;
    }

    public int size() {
        return items.size();
    }

    public CollectionType[] toArray(CollectionType[] destination) {
        int i = 0;
        for (CollectionType item : items) {
            destination[i++] = item;
        }
        return destination;
    }

    public <X> X[] toArray(Activator<X> activator, DefaultConstructor.Constructor1<X, CollectionType> constructor) {
        return ArrayHelper.toArray(items, activator, constructor);
    }

    public ArrayList<CollectionType> toList() {
        ArrayList<CollectionType> list = new ArrayList<CollectionType>();
        for (CollectionType item : items) {
            list.add(item);
        }
        return list;
    }

    @Override
    public Iterator<CollectionType> iterator() {
        return items.iterator();
    }

    @NonNull
    @Override
    public final SavedInstanceStateData saveState() {
        SavedInstanceStateData data = new SavedInstanceStateData();
        onSaveState(data);
        return data;
    }

    @Override
    public final void restoreState(SavedInstanceStateData savedState) {
        onRestoreState(savedState);
    }

    @CallSuper
    protected void onRestoreState(SavedInstanceStateData source) {
        ArrayList<Parcelable> savedItems = source.readObjectArrayList();
        if (savedItems != null && savedItems.size() != 0) {
            items.clear();
            for (Parcelable t : savedItems) {
                @SuppressWarnings({ "unchecked" })
                CollectionType e = (CollectionType) t;
                items.add(e);
            }
        }
        canNext = source.readBoolean();
        page = source.readInt();
        pendingCleared = source.readBoolean();
    }

    @CallSuper
    protected void onSaveState(SavedInstanceStateData dest) {
        ArrayList<Parcelable> suppressUncheckedCastCollection = new ArrayList<>();
        for (CollectionType item : items) {
            if (!(item instanceof Parcelable)) {
                throw new UnsupportedOperationException("Unsupported save state when item is not Parcelable");
            }
            suppressUncheckedCastCollection.add((Parcelable) item);
        }
        dest.writeObjectArrayList(suppressUncheckedCastCollection);
        dest.writeBoolean(canNext);
        dest.writeInt(page);
        dest.writeBoolean(pendingCleared);
    }

    @Override
    public CharSequence getEmptyText() {
        if (!isEnableEmptyRow) {
            return null;
        }
        return fetching ? loadingText : (currentException == null ? emptyText : getEmptyTextByException(currentException));
    }

    protected void setPendingCleared(boolean value) {
        pendingCleared = value;
    }

    protected static abstract class EmptyAdapter extends HeaderRecyclerAdapter {

        public EmptyAdapter(BaseRecyclerAdapter<?> base) {
            super(base);
        }

        @Override
        public int getItemViewType(int position) {
            if (getBaseAdapter().isEnableEmptyRow() && getBaseAdapter().isEmpty()) {
                return HEADER_VIEW_EMPTY;
            }
            return getBaseAdapter().getItemViewType(position);
        }

        @Override
        public int getItemCount() {
            if (getBaseAdapter().isEnableEmptyRow() && getBaseAdapter().isEmpty()) {
                return 1;
            }
            return getBaseAdapter().getItemCount();
        }

        @Override
        public View inflateHeaderView(LayoutInflater inflater, ViewGroup parent) {
            return null;
        }

        @Override
        public void onBindViewHolder(BindableViewHolder holder, int position) {
            if (holder instanceof EmptyViewHolder) {
                super.onBindViewHolder(holder, position);
            } else {
                getBaseAdapter().onBindViewHolder(holder, position);
            }
        }

        @Override
        public abstract View inflateEmptyView(LayoutInflater inflater, ViewGroup parent);
    }
}
