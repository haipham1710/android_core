package tmt.common.designinteraction.presentation;

import android.content.Context;
import android.os.Parcelable;
import android.text.TextUtils;

public abstract class BaseRecyclerAdapter3<T extends Parcelable> extends BaseRecyclerAdapter2<T> {
    public BaseRecyclerAdapter3(Context context) {
        super(context);
    }

    public BaseRecyclerAdapter3(Context context, T empty, T[] items) {
        super(context, empty, items);
    }

    public BaseRecyclerAdapter3(Context context, T[] items) {
        super(context, items);
    }

    public BaseRecyclerAdapter3(Context context, Iterable<T> items) {
        super(context, items);
    }

    public BaseRecyclerAdapter3(Context context, T empty, Iterable<T> items) {
        super(context, empty, items);
    }

    protected String keyword = null;
    private String pendingKeyword = null;
    private boolean hasPendingKeyword = false;

    @Override
    public void reset(String keyword) {
        if (isFetching()) {
            pendingKeyword = keyword;
            hasPendingKeyword = true;
        } else {
            this.keyword = keyword;
            super.reset();
        }
    }

    @Override
    protected void onCompleteFetch() {
        super.onCompleteFetch();
        if (hasPendingKeyword) {
            keyword = pendingKeyword;
            pendingKeyword = null;
            hasPendingKeyword = false;
            reset();
        }
    }

    @Override
    public boolean hasKeyword() {
        return !TextUtils.isEmpty(keyword);
    }
}
