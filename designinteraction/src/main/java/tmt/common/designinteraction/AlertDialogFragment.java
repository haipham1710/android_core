package tmt.common.designinteraction;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

public class AlertDialogFragment<Target extends Fragment> extends DialogFragment {
    private static final String CLASS_NAME = AlertDialogFragment.class.getName();
    public static final String TITLE = CLASS_NAME + ".title";
    public static final String CONTENT = CLASS_NAME + ".content";
    public static final String POSITIVE = CLASS_NAME + ".positive";
    public static final String NEUTRAL = CLASS_NAME + ".neutral";
    public static final String NEGATIVE = CLASS_NAME + ".negative";
    public static final String INPUT_HINT = CLASS_NAME + ".input-hint";
    public static final String INPUT_PREFILL = CLASS_NAME + ".input-prefill";
    public static final String INPUT_ALLOW_EMPTY = CLASS_NAME + ".input-allow-empty";

    public final Bundle ensureArgs() {
        Bundle args = getArguments();
        if (args == null) {
            args = new Bundle();
            setArguments(args);
        }
        return args;
    }

    protected void show(Target target) {
        setTargetFragment(target, 0);
        show(target.getFragmentManager(), null);
    }

    protected void show(FragmentActivity activity) {
        show(activity.getSupportFragmentManager(), null);
    }

    public void setTitle(CharSequence title) {
        ensureArgs().putCharSequence(TITLE, title);
    }

    public CharSequence getTitle() {
        return ensureArgs().getCharSequence(TITLE);
    }

    public void setContent(CharSequence title) {
        ensureArgs().putCharSequence(CONTENT, title);
    }

    public CharSequence getContent() {
        return ensureArgs().getCharSequence(CONTENT);
    }

    public void setPositive(CharSequence title) {
        ensureArgs().putCharSequence(POSITIVE, title);
    }

    public CharSequence getPositive() {
        return ensureArgs().getCharSequence(POSITIVE);
    }

    public void setNegative(CharSequence title) {
        ensureArgs().putCharSequence(NEGATIVE, title);
    }

    public CharSequence getNegative() {
        return ensureArgs().getCharSequence(NEGATIVE);
    }

    public void setNeutral(CharSequence title) {
        ensureArgs().putCharSequence(NEUTRAL, title);
    }

    public CharSequence getNeutral() {
        return ensureArgs().getCharSequence(NEUTRAL);
    }

    @NonNull
    @Override
    public MaterialDialog onCreateDialog(Bundle savedInstanceState) {
        CharSequence title = getTitle();
        CharSequence content = getContent();
        CharSequence positive = getPositive();
        CharSequence neutral = getNeutral();
        CharSequence negative = getNegative();
        CharSequence inputHint = getArguments().getString(INPUT_HINT);
        MaterialDialog.Builder builder = new MaterialDialog.Builder(getActivity());
        if (content != null) {
            builder.content(content);
        }
        if (title != null) {
            builder.title(title);
        }
        if (positive != null) {
            builder.positiveText(positive);
        }
        if (negative != null) {
            builder.negativeText(negative);
        }
        if (neutral != null) {
            builder.neutralText(neutral);
        }
        if (!TextUtils.isEmpty(inputHint)) {
            String prefill = getArguments().getString(INPUT_PREFILL);
            boolean allowEmpty = getArguments().getBoolean(INPUT_ALLOW_EMPTY);
            builder.input(inputHint, prefill, allowEmpty, mInputCallback);
        }
        builder.onAny(mCallback);
        onModifyBuilder(builder);
        return builder.build();
    }

    public void setInput(@NonNull CharSequence hint, CharSequence prefill, boolean allowEmpty) {
        Bundle args = ensureArgs();
        args.putCharSequence(INPUT_HINT, hint);
        args.putCharSequence(INPUT_PREFILL, prefill);
        args.putBoolean(INPUT_ALLOW_EMPTY, allowEmpty);
    }

    @Override
    public MaterialDialog getDialog() {
        return (MaterialDialog) super.getDialog();
    }

    protected void onModifyBuilder(MaterialDialog.Builder builder) {

    }

    private MaterialDialog.SingleButtonCallback mCallback = new MaterialDialog.SingleButtonCallback() {
        @Override
        public void onClick(@NonNull MaterialDialog materialDialog, @NonNull DialogAction dialogAction) {
            @SuppressWarnings({ "unchecked" })
            Target target = (Target) getTargetFragment();
            switch (dialogAction) {
                case NEGATIVE:
                    onNegative(target);
                    break;
                case POSITIVE:
                    onPositive(target);
                    break;
                case NEUTRAL:
                    onNeutral(target);
                    break;
            }
        }
    };
    private MaterialDialog.InputCallback mInputCallback = new MaterialDialog.InputCallback() {
        @Override
        public void onInput(@NonNull MaterialDialog materialDialog, CharSequence charSequence) {
            AlertDialogFragment.this.onInput(getTypedTargetFragment(), charSequence);
        }
    };

    protected void onInput(Target target, CharSequence charSequence) {

    }


    protected void onNeutral(Target target) {
    }

    protected void onPositive(Target target) {
    }

    protected void onNegative(Target target) {
    }

    @SuppressWarnings({ "unchecked" })
    public Target getTypedTargetFragment() {
        return (Target) getTargetFragment();
    }

    public boolean isFinishing() {
        FragmentActivity activity = getActivity();
        return activity == null || activity.isFinishing();
    }
}
