package tmt.common;

public class NeverHappenException extends RuntimeException {

    /**
     * Use for default case of switch statement or if statement if this case's never reach
     */
    private static final long serialVersionUID = -7217801696953305809L;

    public NeverHappenException() {
        super();
    }

    public NeverHappenException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public NeverHappenException(String detailMessage) {
        super(detailMessage);
    }

    public NeverHappenException(Throwable throwable) {
        super(throwable);
    }

}
