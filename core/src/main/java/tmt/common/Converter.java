package tmt.common;

import java.util.Date;

public class Converter {

    public static Date dateFrom(long milliseconds) {
        if (milliseconds == 0) {
            return null;
        }
        return new Date(milliseconds);
    }
}
