package tmt.common.util;

import tmt.common.presentation.FetchableAdapter;

public abstract class EmptyFetchingListener implements FetchableAdapter.FetchingListener {
    @Override
    public void onBeginFetch() {

    }

    @Override
    public void onCompleteFetch() {

    }

    @Override
    public void hasException(Exception e) {

    }
}
