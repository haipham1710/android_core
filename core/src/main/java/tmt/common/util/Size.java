package tmt.common.util;

import android.os.Parcel;
import android.os.Parcelable;

public class Size implements Parcelable {
    public int width;
    public int height;

    public Size() {

    }

    public Size(int width, int height) {
        this.width = width;
        this.height = height;
    }

    protected Size(Parcel in) {
        width = in.readInt();
        height = in.readInt();
    }

    public static final Creator<Size> CREATOR = new Creator<Size>() {
        @Override
        public Size createFromParcel(Parcel in) {
            return new Size(in);
        }

        @Override
        public Size[] newArray(int size) {
            return new Size[size];
        }
    };

    public float ratio() {
        if (height == 0) {
            return 0;
        }
        return (float) width / height;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(width);
        dest.writeInt(height);
    }
}
