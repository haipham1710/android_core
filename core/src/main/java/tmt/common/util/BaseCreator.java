package tmt.common.util;

import android.os.Parcel;
import android.os.Parcelable;

import tmt.common.communication.ObjectReader;
import tmt.common.communication.Reader;
import tmt.common.datacontract.DataCreator;
import tmt.common.datacontract.JsonHelper;

public abstract class BaseCreator<T> implements Parcelable.Creator<T>, DataCreator<T>, ObjectReader<T> {

    @Override
    public T createFromReader(Reader reader) {
        throw new UnsupportedOperationException("Implements in child class");
    }

    @Override
    public T createFromJson(JsonHelper helper) {
        throw new UnsupportedOperationException("Implements in child class");
    }

    @Override
    public T createFromParcel(Parcel source) {
        throw new UnsupportedOperationException("Implements in child class");
    }

    @Override
    public abstract T[] newArray(int size);

}
