package tmt.common.util;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import tmt.common.Arrays;
import tmt.common.presentation.DefaultConstructor;

public class ParcelableString implements Parcelable {
    public final String value;

    protected ParcelableString(Parcel in) {
        value = in.readString();
    }

    public ParcelableString(String value) {
        this.value = value;
    }

    public static final Creator<ParcelableString> CREATOR = new Creator<ParcelableString>() {
        @Override
        public ParcelableString createFromParcel(Parcel in) {
            return new ParcelableString(in);
        }

        @Override
        public ParcelableString[] newArray(int size) {
            return new ParcelableString[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(value);
    }

    public static ParcelableString[] from(String[] values) {
        if (values == null) {
            return null;
        }
        return Arrays.map(values, new ParcelableString[values.length], new DefaultConstructor.Constructor1<ParcelableString, String>() {
            @NonNull
            @Override
            public ParcelableString newInstance(String p1) {
                return new ParcelableString(p1);
            }
        });
    }
}
