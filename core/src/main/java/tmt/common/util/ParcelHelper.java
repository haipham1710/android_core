package tmt.common.util;

import android.os.Parcel;

import java.util.Date;

public class ParcelHelper {
    private final Parcel base;

    public ParcelHelper(Parcel base) {
        this.base = base;
    }

    public boolean readBoolean() {
        return base.readInt() != 0;
    }

    public void writeBoolean(boolean value) {
        base.writeInt(value ? 1 : 0);
    }

    public Date readDate() {
        boolean hasValue = readBoolean();
        if (hasValue) {
            return new Date(base.readLong());
        }
        return null;
    }

    public void writeDate(Date value) {
        writeBoolean(value != null);
        if (value != null) {
            base.writeLong(value.getTime());
        }
    }

    public void writeNullableInt(Integer value) {
        writeBoolean(value != null);
        if (value != null) {
            base.writeInt(value);
        }
    }

    public Integer readNullableInt() {
        boolean hasValue = readBoolean();
        if (hasValue) {
            return base.readInt();
        }
        return null;
    }

    public void writeNullableLong(Long value) {
        writeBoolean(value != null);
        if (value != null) {
            base.writeLong(value);
        }
    }

    public Long readNullableLong() {
        boolean hasValue = readBoolean();
        if (hasValue) {
            return base.readLong();
        }
        return null;
    }

    public void writeNullableBoolean(Boolean value) {
        if (value == null) {
            base.writeInt(0);
        } else {
            base.writeInt(value ? 1 : -1);
        }
    }

    public Boolean readNullableBoolean() {
        int value = base.readInt();
        return value == 0 ? null : (value > 0);
    }
}
