package tmt.common.util;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;


public class SavedInstanceStateData implements Parcelable {
    private final Bundle bundle;
    private int writeId = 0;
    private int readId = 0;

    public SavedInstanceStateData() {
        bundle = new Bundle();
    }

    public static final Creator<SavedInstanceStateData> CREATOR = new Creator<SavedInstanceStateData>() {
        @Override
        public SavedInstanceStateData createFromParcel(Parcel in) {
            return new SavedInstanceStateData(in);
        }

        @Override
        public SavedInstanceStateData[] newArray(int size) {
            return new SavedInstanceStateData[size];
        }
    };

    private String nextWriteId() {
        return String.valueOf(++writeId);
    }

    private String nextReadId() {
        return String.valueOf(++readId);
    }

    public void writeInt(int value) {
        bundle.putInt(nextWriteId(), value);
    }

    public void writeLong(long value) {
        bundle.putLong(nextWriteId(), value);
    }

    public void writeString(String value) {
        bundle.putString(nextWriteId(), value);
    }

    public void writeBoolean(boolean value) {
        bundle.putInt(nextWriteId(), value ? 1 : 0);
    }

    public int readInt() {
        return bundle.getInt(nextReadId());
    }

    public long readLong() {
        return bundle.getLong(nextReadId());
    }

    public boolean readBoolean() {
        return bundle.getInt(nextReadId()) != 0;
    }

    public String readString() {
        return bundle.getString(nextReadId());
    }

    public <T extends Parcelable> void writeObject(T object) {
        bundle.putParcelable(nextWriteId(), object);
    }

    public <T extends Parcelable> T readObject() {
        return bundle.getParcelable(nextReadId());
    }

    public void writeObjectArrayList(ArrayList<? extends Parcelable> object) {
        bundle.putParcelableArrayList(nextWriteId(), object);
    }

    public <T extends Parcelable> ArrayList<T> readObjectArrayList() {
        return bundle.getParcelableArrayList(nextReadId());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeBundle(bundle);
        dest.writeInt(writeId);
        dest.writeInt(readId);
    }

    protected SavedInstanceStateData(Parcel in) {
        bundle = in.readBundle();
        writeId = in.readInt();
        readId = in.readInt();
    }
}
