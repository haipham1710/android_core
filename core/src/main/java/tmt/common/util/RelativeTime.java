package tmt.common.util;

import android.support.annotation.NonNull;
import android.text.format.DateUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class RelativeTime {
    private static final SimpleDateFormat sameMonthFormat = new SimpleDateFormat("E, d", Locale.US);
    private static final SimpleDateFormat sameYearFormat = new SimpleDateFormat("E, d/M", Locale.US);
    private static final SimpleDateFormat fullFormat = new SimpleDateFormat("E, d/M/yy", Locale.US);
    private static final SimpleDateFormat timeFormat = new SimpleDateFormat("h:mm a", Locale.US);

    @NonNull
    public static String relativeDate(Date date) {
        if (date == null) {
            return "";
        }
        Calendar now = Calendar.getInstance();
        Calendar value = Calendar.getInstance();
        value.setTime(date);
        if (isSameDay(value, now)) {
            return "Today";
        }
        if (isYesterday(value, now)) {
            return "Yesterday";
        }
        if (isSameMonth(value, now)) {
            return sameMonthFormat.format(date);
        }
        if (isSameYear(value, now)) {
            return sameYearFormat.format(date);
        }
        return fullFormat.format(date);
    }

    @NonNull
    public static CharSequence relativeTime(Date time) {
        if (time == null) {
            return "";
        }
        Date now = new Date();
        long durationInMillis = now.getTime() - time.getTime();
        if (0 <= durationInMillis && durationInMillis <= DateUtils.DAY_IN_MILLIS) {
            return DateUtils.getRelativeTimeSpanString(time.getTime(), now.getTime(), DateUtils.MINUTE_IN_MILLIS);
        }
        // Don't use relative time if duration is greater a day
        return timeFormat.format(time);
    }

    public static boolean isSameDay(Date from, Date to) {
        Calendar a = Calendar.getInstance();
        Calendar b = Calendar.getInstance();
        a.setTime(from);
        b.setTime(to);
        return isSameDay(a, b);
    }

    public static boolean isSameDay(Calendar from, Calendar to) {
        return from.get(Calendar.DATE) == to.get(Calendar.DATE) &&
                from.get(Calendar.MONTH) == to.get(Calendar.MONTH) &&
                from.get(Calendar.YEAR) == to.get(Calendar.YEAR);
    }

    public static boolean isSameMonth(Calendar from, Calendar to) {
        return from.get(Calendar.MONTH) == to.get(Calendar.MONTH) &&
                from.get(Calendar.YEAR) == to.get(Calendar.YEAR);
    }

    public static boolean isSameYear(Calendar from, Calendar to) {
        return from.get(Calendar.YEAR) == to.get(Calendar.YEAR);
    }

    public static boolean isYesterday(Calendar from, Calendar to) {
        if (isSameYear(from, to)) {
            return from.get(Calendar.DAY_OF_YEAR) + 1 == to.get(Calendar.DAY_OF_YEAR);
        }
        return (from.get(Calendar.YEAR) + 1) == to.get(Calendar.YEAR) &&
                to.get(Calendar.DAY_OF_YEAR) == 1 &&
                from.get(Calendar.MONTH) == 12 && from.get(Calendar.DATE) == 31;
    }
}
