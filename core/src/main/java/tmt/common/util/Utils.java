package tmt.common.util;

import android.content.Context;
import android.util.DisplayMetrics;

import java.util.Date;

public class Utils {

    public static Date dateFrom(long totalMilliseconds) {
        if (totalMilliseconds <= 0) {
            return null;
        }
        return new Date(totalMilliseconds);
    }

    public static int dpToPx(Context context, int dp) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }
}
