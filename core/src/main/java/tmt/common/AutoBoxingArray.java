package tmt.common;

public final class AutoBoxingArray {

    public static Integer[] box(int[] value) {
        if (value == null) {
            return null;
        }
        Integer[] result = new Integer[value.length];
        for (int i = 0; i < value.length; i++) {
            result[i] = value[i];
        }
        return result;
    }

    public static int[] unbox(Integer[] value) {
        if (value == null) {
            return null;
        }
        int[] result = new int[value.length];
        for (int i = 0; i < value.length; i++) {
            result[i] = value[i];
        }
        return result;
    }
}
