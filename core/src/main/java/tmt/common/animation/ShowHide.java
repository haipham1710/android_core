package tmt.common.animation;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.view.View;

import java.util.ArrayList;

public class ShowHide {
    private static final class Holder {
        private final boolean useTransparent;
        private final float factorX;
        private final float factorY;
        private final float distanceY;
        private final float distanceX;
        private final View view;

        public Holder(View view, float factorX, float distanceX, float factorY, float distanceY, boolean useTransparent) {
            this.view = view;
            this.factorX = factorX;
            this.distanceX = distanceX;
            this.factorY = factorY;
            this.distanceY = distanceY;
            this.useTransparent = useTransparent;
        }
    }

    private final ArrayList<Holder> animations = new ArrayList<>();
    private final long duration;
    private AnimatorSet hideSet;
    private AnimatorSet showSet;

    public ShowHide(Context context, long duration) {
        this.duration = duration;
    }

    public ShowHide add(View view, float factorX, float distanceX, float factorY, float distanceY, boolean useTransparent) {
        animations.add(new Holder(view, factorX, distanceX, factorY, distanceY, useTransparent));
        return this;
    }

    public ShowHide add(View view, float distanceX, float distanceY, boolean useTransparent) {
        return add(view, 0, distanceX, 0, distanceY, useTransparent);
    }

    public ShowHide addMoveRight(View view, float additionalDistanceX, boolean useTransparent) {
        return add(view, 1, additionalDistanceX, 0, 0, useTransparent);
    }

    private boolean make() {
        ArrayList<ObjectAnimator> hides = new ArrayList<>();
        ArrayList<ObjectAnimator> shows = new ArrayList<>();
        for (Holder item : animations) {
            if (item.distanceX != 0 || item.factorX != 0) {
                hides.add(ObjectAnimator.ofFloat(item.view, View.TRANSLATION_X, item.factorX * item.view.getWidth() + item.distanceX));
                shows.add(ObjectAnimator.ofFloat(item.view, View.TRANSLATION_X, 0));
            }
            if (item.distanceY != 0 || item.factorY != 0) {
                hides.add(ObjectAnimator.ofFloat(item.view, View.TRANSLATION_Y, item.factorY * item.view.getHeight() + item.distanceY));
                shows.add(ObjectAnimator.ofFloat(item.view, View.TRANSLATION_Y, 0));
            }
            if (item.useTransparent) {
                hides.add(ObjectAnimator.ofFloat(item.view, View.ALPHA, 0));
                shows.add(ObjectAnimator.ofFloat(item.view, View.ALPHA, 1));
            }
        }
        ObjectAnimator[] hideAnimations = new ObjectAnimator[hides.size()];
        ObjectAnimator[] showAnimations = new ObjectAnimator[shows.size()];

        hides.toArray(hideAnimations);
        shows.toArray(showAnimations);

        hideSet = new AnimatorSet();
        hideSet.playTogether(hideAnimations);
        hideSet.setDuration(duration);
        showSet = new AnimatorSet();
        showSet.playTogether(showAnimations);
        showSet.setDuration(duration);
        return true;
    }

    public final void show() {
        make();
        showSet.start();
    }

    public final void hide() {
        make();
        hideSet.start();
    }

}
