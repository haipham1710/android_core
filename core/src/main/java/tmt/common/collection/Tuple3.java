package tmt.common.collection;

public class Tuple3<T1, T2, T3> extends Tuple2<T1, T2> {
    public T3 item3;

    public Tuple3() {
        super();
    }

    public Tuple3(T1 item1, T2 item2) {
        super(item1, item2);
    }

    public Tuple3(T1 item1, T2 item2, T3 item3) {
        this(item1, item2);
        this.item3 = item3;
    }

}
