package tmt.common.communication;

public enum SocketStatus {
    CannotConnect("Không kết nối được thiết bị"),
    ConnectionTimeout("Kết nối hết thời gian"),
    ParseError("Internal Error"),
    Ok("Không đọc được dữ liệu từ thiết bị");

    private final String description;

    SocketStatus(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return description;
    }
}
