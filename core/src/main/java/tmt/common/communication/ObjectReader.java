package tmt.common.communication;

public interface ObjectReader<T> {

    T createFromReader(Reader reader);

    T[] newArray(int size);
}
