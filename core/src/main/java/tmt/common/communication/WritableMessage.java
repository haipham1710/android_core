package tmt.common.communication;

import android.text.TextUtils;

public abstract class WritableMessage implements WritableObject {
    public int sequenceId;
    private String name;

    public abstract void writeToWriter(Writer writer);

    protected WritableMessage() {
        Class<? extends WritableMessage> clazz = getClass();
        if (clazz.isAnnotationPresent(Writable.class)) {
            Writable attr = clazz.getAnnotation(Writable.class);
            name = attr.name();
        }
        if (TextUtils.isEmpty(name)) {
            name = clazz.getSimpleName();
        }
    }

    public String getMessage() {
        Writer writer = new Writer();
        writeToWriter(writer);
        String content = writer.getContent();
        if (!TextUtils.isEmpty(content)) {
            return name + " " + sequenceId + " " + content;
        }
        return name + " " + sequenceId;
    }
}
