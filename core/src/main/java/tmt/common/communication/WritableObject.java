package tmt.common.communication;

public interface WritableObject {
    void writeToWriter(Writer writer);
}
