package tmt.common.communication;

import android.text.TextUtils;

import tmt.common.StringHelper;

@Deprecated
public class ResponseReader {

    private String content;

    public ResponseReader(String content) {
        this.content = content;
    }

    public final boolean canRead() {
        return !TextUtils.isEmpty(content);
    }

    private String readNext() {
        if (!canRead()) {
            return null;
        }
        int index = content.indexOf(',');
        if (index != -1) {
            String value = content.substring(0, index);
            content = content.substring(index + 1);
            return value;
        } else {
            String value = content;
            content = null;
            return value;
        }
    }

    public final int readInt() {
        String value = readNext();
        if (value != null) {
            return Integer.parseInt(value);
        }
        return 0;
    }

    public final long readLong() {
        String value = readNext();
        if (value != null) {
            return Long.parseLong(value);
        }
        return 0;
    }

    public final boolean readBoolean() {
        String value = readNext();
        return value != null && "1".equals(value);
    }

    public final String readString() {
        return StringHelper.decode(readNext());
    }

}
