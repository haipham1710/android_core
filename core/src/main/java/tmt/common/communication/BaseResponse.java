package tmt.common.communication;

/**
 * Deprecated, use {@code ReadableMessage} instead.
 *
 * @author Starfall
 */
@Deprecated
public abstract class BaseResponse {
    protected interface ResponsePopulater<T extends BaseResponse> {
        T responseOfId(int id);
    }

    public int forRandom;
    private int responseId;

    public int getResponseId() {
        return responseId;
    }

    public BaseResponse(int id) {
        responseId = id;
    }

    public final void from(String content) {
        read(new ResponseReader(content));
    }


    protected abstract void read(ResponseReader reader);

    protected static <T extends BaseResponse> T parse(String message, ResponsePopulater<T> populater) throws CommunicationException {
        int index = message.indexOf(' ');
        int id = Integer.parseInt(message.substring(0, index));
        message = message.substring(index + 1);

        index = message.indexOf(' ');
        int forRandom = Integer.parseInt(index < 0 ? message : message.substring(0, index));
        String content = index < 0 ? null : message.substring(index + 1);
        T response = populater.responseOfId(id);
        try {
            response.from(content);
        } catch (Exception e) {
            throw new CommunicationException("Error when parsing the message: " + message, e);
        }
        response.forRandom = forRandom;
        return response;
    }

}
