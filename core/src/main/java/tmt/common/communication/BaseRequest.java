package tmt.common.communication;

@Deprecated
public abstract class BaseRequest {

    public int random;
    private int requestId;

    protected abstract void toContent(RequestWriter writer);

    public String getMessage() {
        RequestWriter writer = new RequestWriter();
        toContent(writer);
        String content = writer.getContent();
        if (content != null && content.length() != 0) {
            return String.valueOf(requestId) + " " + String.valueOf(random) + " " + content;
        }
        return String.valueOf(requestId) + " " + String.valueOf(random);
    }

    public BaseRequest(int id) {
        this.requestId = id;
    }

    public int getRequestId() {
        return requestId;
    }

}
