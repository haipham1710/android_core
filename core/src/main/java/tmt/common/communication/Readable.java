package tmt.common.communication;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Dictionary;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Readable {
    String name() default "";

    final class Helper {
        public static Dictionary<String, Class<?>> collectAttributes() {
            throw new UnsupportedOperationException();
        }
    }
}
