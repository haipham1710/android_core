package tmt.common.communication;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.Date;

import tmt.common.StringHelper;

public class Reader {
    private String _content;

    public Reader(String content) {
        _content = content;
    }

    public boolean canRead() {
        return !TextUtils.isEmpty(_content);
    }

    private String readNext() {
        if (TextUtils.isEmpty(_content)) {
            return null;
        }
        int index = _content.indexOf(',');
        if (index != -1) {
            String value = _content.substring(0, index);
            _content = _content.substring(index + 1);
            return value;
        } else {
            String value = _content;
            _content = null;
            return value;
        }
    }

    public long readLong() {
        String value = readNext();
        if (value != null) {
            return Long.parseLong(value);
        }
        return 0L;
    }

    public int readInt() {
        String value = readNext();
        if (value != null) {
            return Integer.parseInt(value);
        }
        return 0;
    }

    public double readDouble() {
        String value = readNext();
        if (value != null) {
            return Double.parseDouble(value);
        }
        return 0;
    }

    public boolean readBoolean() {
        String value = readNext();
        return "1".equals(value);
    }

    public String readString() {
        return StringHelper.decode(readNext());
    }

    public Date readDate() {
        long milliseconds = readLong();
        if (milliseconds < 0) {
            return null;
        }
        return new Date(milliseconds);
    }

    public <T> T readObject(ObjectReader<T> creator) {
        if (readBoolean()) {
            return creator.createFromReader(this);
        }
        return null;
    }

    public <T> T[] readArrayObject(ObjectReader<T> creator) {
        boolean hasValue = readBoolean();
        if (hasValue) {
            T[] value = creator.newArray(readInt());
            for (int i = 0; i < value.length; i++) {
                value[i] = creator.createFromReader(this);
            }
            return value;
        }
        return null;
    }

    public String[] readStringArray() {
        boolean hasValue = readBoolean();
        if (hasValue) {
            String[] value = new String[readInt()];
            for (int i = 0; i < value.length; i++) {
                value[i] = readString();
            }
            return value;
        }
        return null;
    }

    public <T> ArrayList<T> readArrayListObject(ObjectReader<T> creator) {
        boolean hasValue = readBoolean();
        if (hasValue) {
            int len = readInt();
            ArrayList<T> array = new ArrayList<>(len);
            for (int i = 0; i < len; i++) {
                array.add(creator.createFromReader(this));
            }
            return array;
        }
        return null;
    }
}
