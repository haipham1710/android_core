package tmt.common.communication;

import android.text.TextUtils;

import java.lang.reflect.Field;
import java.util.Hashtable;

import tmt.common.presentation.Activator;
import tmt.common.presentation.UndeclaredFieldException;

public abstract class ReadableMessage {
    private int sequenceId;

    public int sequenceId() {
        return sequenceId;
    }

    public abstract void readFromReader(Reader reader);

    private static final Hashtable<String, Activator<?>> factors = new Hashtable<>();

    private void from(String content) {
        readFromReader(new Reader(content));
    }

    public static <T extends ReadableMessage> void register(Class<T> clazz, Activator<T> field) {
        String name = null;
        if (clazz.isAnnotationPresent(Readable.class)) {
            Readable attr = clazz.getAnnotation(Readable.class);
            name = attr.name();
        }
        if (TextUtils.isEmpty(name)) {
            name = clazz.getSimpleName();
        }
        if (field != null) {
            factors.put(name, field);
            return;
        }
        Field f;
        Activator<?> value;
        try {
            f = clazz.getField("ACTIVATOR");
        } catch (NoSuchFieldException e) {
            f = null;
        }
        if (f != null) {
            try {
                Object obj = f.get(null);
                if (obj instanceof Activator<?>) {
                    value = (Activator<?>) obj;
                } else {
                    throw new UndeclaredFieldException("ACTIVATOR", Activator.class, clazz);
                }
            } catch (Exception e) {
                throw new UndeclaredFieldException("ACTIVATOR", Activator.class, clazz);
            }
        } else {
            throw new UndeclaredFieldException("ACTIVATOR", Activator.class, clazz);
        }
        factors.put(name, value);
    }

    public static <T extends ReadableMessage> void register(Class<T> clazz) {
        register(clazz, null);
    }

    public static <T extends ReadableMessage> T parseBase(String message) {
        int index = message.indexOf(' ');
        String name = message.substring(0, index);
        message = message.substring(index + 1);

        index = message.indexOf(' ');
        int sequenceId = Integer.parseInt(index < 0 ? message : message.substring(0, index));
        String content = index < 0 ? null : message.substring(index + 1);

        @SuppressWarnings("unchecked")
        T request = (T) createInstanceForName(name);
        if (request != null) {
            @SuppressWarnings({ "inline" })
            ReadableMessage privateAccessibleObject = request;
            privateAccessibleObject.from(content);
            privateAccessibleObject.sequenceId = sequenceId;
        }
        return request;
    }

    private static Object createInstanceForName(String requestName) {
        Activator<?> clazz = factors.get(requestName);
        if (clazz == null) {
            return null;
        }
        return clazz.newInstance();
    }
}
