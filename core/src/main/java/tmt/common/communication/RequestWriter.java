package tmt.common.communication;

import tmt.common.StringHelper;

@Deprecated
public class RequestWriter {
    private final StringBuilder sb = new StringBuilder();

    public final RequestWriter put(double value) {
        if (sb.length() != 0) {
            sb.append(',');
        }
        sb.append(String.valueOf(value));
        return this;
    }

    public final RequestWriter put(int value) {
        if (sb.length() != 0) {
            sb.append(',');
        }
        sb.append(String.valueOf(value));
        return this;
    }

    public final RequestWriter put(boolean value) {
        if (sb.length() != 0) {
            sb.append(',');
        }
        sb.append(value ? "1" : "0");
        return this;
    }

    public final RequestWriter put(String value) {
        if (sb.length() != 0) {
            sb.append(',');
        }
        sb.append(StringHelper.encode(value));
        return this;
    }

    String getContent() {
        return sb.toString();
    }

}
