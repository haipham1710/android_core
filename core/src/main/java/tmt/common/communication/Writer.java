package tmt.common.communication;

import java.util.Date;
import java.util.List;

import tmt.common.StringHelper;

public final class Writer {

    private final StringBuilder sb = new StringBuilder();

    public Writer write(int value) {
        appendSeparator();
        sb.append(value);
        return this;
    }

    public Writer write(long value) {
        appendSeparator();
        sb.append(value);
        return this;
    }

    public Writer write(boolean value) {
        appendSeparator();
        sb.append(value ? "1" : "0");
        return this;
    }

    public Writer write(String value) {
        appendSeparator();
        if (value != null) {
            sb.append(StringHelper.encode(value));
        }
        return this;
    }

    public Writer write(Date value) {
        long milliseconds = value == null ? -1 : value.getTime();
        write(milliseconds);
        return this;
    }

    public Writer write(WritableObject value) {
        write(value != null);
        if (value != null) {
            value.writeToWriter(this);
        }
        return this;
    }

    public Writer write(WritableObject[] value) {
        write(value != null);
        if (value != null) {
            write(value.length);
            for (WritableObject obj : value) {
                obj.writeToWriter(this);
            }
        }
        return this;
    }

    public Writer write(String[] value) {
        write(value != null);
        if (value != null) {
            write(value.length);
            for (String obj : value) {
                write(obj);
            }
        }
        return this;
    }

    public <T extends WritableObject> Writer write(List<T> value) {
        write(value != null);
        if (value != null) {
            write(value.size());
            for (WritableObject obj : value) {
                obj.writeToWriter(this);
            }
        }
        return this;
    }

    public Writer write(int[] value) {
        write(value != null);
        if (value != null) {
            write(value.length);
            for (int i : value) {
                write(i);
            }
        }
        return this;
    }

    public String getContent() {
        return sb.toString();
    }

    @Override
    public String toString() {
        return getContent();
    }

    private boolean hasValue = false;

    private void appendSeparator() {
        if (hasValue) {
            sb.append(',');
        } else {
            hasValue = true;
        }
    }
}
