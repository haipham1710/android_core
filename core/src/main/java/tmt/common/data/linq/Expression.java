package tmt.common.data.linq;

import android.support.annotation.NonNull;

public abstract class Expression<T> implements Expressible {

    @NonNull
    @Override
    public abstract String clause();

    @NonNull
    @Override
    public abstract Object[] args();

    public Expression<T> lessThan(T value) {
        return new BinaryExpression<>(BinaryExpression.Operator.LESS_THAN, this, new LiteralExpression(value));
    }

    public Expression<T> lessThanOrEquals(T value) {
        return new BinaryExpression<>(BinaryExpression.Operator.LESS_THAN_OR_EQUALS, this, new LiteralExpression(value));
    }

    /**
     * We can not name it to "equals" because this is a method of object
     */
    public Expression<T> eq(T value) {
        return new BinaryExpression<>(BinaryExpression.Operator.EQUALS, this, new LiteralExpression(value));
    }
    public Expression<T> notEquals(T value) {
        return new BinaryExpression<>(BinaryExpression.Operator.NOT_EQUALS, this, new LiteralExpression(value));
    }

    public Expression<T> greaterThanOrEquals(T value) {
        return new BinaryExpression<>(BinaryExpression.Operator.GREATER_OR_EQUALS, this, new LiteralExpression(value));
    }

    public Expression<T> greaterThan(T value) {
        return new BinaryExpression<>(BinaryExpression.Operator.GREATER_THAN, this, new LiteralExpression(value));
    }
}
