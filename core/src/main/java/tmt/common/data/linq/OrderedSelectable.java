package tmt.common.data.linq;

import android.support.annotation.NonNull;

public class OrderedSelectable extends Selectable {

    private final Selectable source;
    private final NamedExpressible[] orderBy;

    public OrderedSelectable(Selectable source, NamedExpressible[] orderBy) {
        this.source = source;
        this.orderBy = orderBy;
    }

    @Override
    public NamedExpressible[] allExpressible() {
        return source.allExpressible();
    }

    @NonNull
    @Override
    public String clause() {
        StringBuilder sb = new StringBuilder(source.clause());
        sb.append(" ORDER BY ");
        for (int i = 0; i < orderBy.length; i++) {
            if (i != 0) {
                sb.append(", ");
            }
            sb.append(orderBy[i].name());
        }
        return sb.toString();
    }

    @NonNull
    @Override
    public Object[] args() {
        return source.args();
    }
}
