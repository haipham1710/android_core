package tmt.common.data.linq;

import android.support.annotation.NonNull;

public interface Expressible {

    @NonNull
    String clause();

    @NonNull
    Object[] args();
}
