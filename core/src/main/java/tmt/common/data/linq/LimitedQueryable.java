package tmt.common.data.linq;

import android.support.annotation.NonNull;

public class LimitedQueryable extends Selectable {
    private final int limit;
    private final int offset;
    @NonNull
    private final Selectable source;

    public LimitedQueryable(@NonNull Selectable source, int limit, int offset) {
        this.source = source;
        this.limit = limit;
        this.offset = offset;
    }

    @NonNull
    @Override
    public String clause() {
        return source.clause() + " LIMIT " + limit + " OFFSET " + offset;
    }

    @NonNull
    @Override
    public Object[] args() {
        return source.args();
    }

    @Override
    public NamedExpressible[] allExpressible() {
        return source.allExpressible();
    }
}
