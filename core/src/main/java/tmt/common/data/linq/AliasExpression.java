package tmt.common.data.linq;

import android.support.annotation.NonNull;

/**
 * Created by Starfall on 11/3/2015.
 */
public class AliasExpression implements NamedExpressible {
    @NonNull
    @Override
    public String name() {
        return null;
    }

    @NonNull
    @Override
    public NamedExpressible desc() {
        return null;
    }

    @NonNull
    @Override
    public NamedExpressible asc() {
        return null;
    }

    @NonNull
    @Override
    public String clause() {
        return null;
    }

    @NonNull
    @Override
    public Object[] args() {
        return new Object[0];
    }
}
