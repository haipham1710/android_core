package tmt.common.data.linq;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import tmt.common.Arrays;
import tmt.common.data.DependencyObject;
import tmt.common.data.sqlite.Column;
import tmt.common.data.sqlite.Selector;
import tmt.common.presentation.DefaultConstructor;

/**
 * Should be interface but we need extension method
 */
public abstract class Selectable implements Expressible {
    public static final String OPTION_DISTINCT = "DISTINCT";
    public static final String OPTION_ALL = "ALL";

    public abstract NamedExpressible[] allExpressible();

    private String[] options = new String[]{};

    public String[] options() {
        return options;
    }

    /**
     * extension method
     *
     */
    public final Selectable orderBy(NamedExpressible... expressible) {
        return new OrderedSelectable(this, expressible);
    }

    /**
     * extension method
     *
     */
    public final Selectable limit(int limit, int offset) {
        return new LimitedQueryable(this, limit, offset);
    }

    public Selectable distinct() {
        options = new String[]{ OPTION_DISTINCT };
        return this;
    }

    public Selectable all() {
        options = new String[]{ OPTION_ALL };
        return this;
    }

    public final Selectable select(final NamedExpressible... expressible) throws IllegalArgumentException {
        checkExpressions(expressible);
        return new Selectable() {
            {
                options = Selectable.this.options;
            }

            @Override
            public NamedExpressible[] allExpressible() {
                return expressible;
            }

            @NonNull
            @Override
            public String clause() {
                return Selectable.this.clause();
            }

            @NonNull
            @Override
            public Object[] args() {
                return Selectable.this.args();
            }
        };
    }

    private void checkExpressions(NamedExpressible[] expressions) throws IllegalArgumentException {
        final NamedExpressible[] allowedExpressible = allExpressible();
        for (NamedExpressible expression : expressions) {
            if (!Arrays.contains(allowedExpressible, expression)) {
                throw new IllegalArgumentException(expression + " isn't contained in current selectable");
            }
        }
    }

    /**
     * extension method
     *
     */
    public final Cursor fetch(SQLiteDatabase database) {
        String query = clause();
        Object[] args = args();
        String[] selectionArgs = Arrays.map(args, new String[args.length], new DefaultConstructor.Constructor1<String, Object>() {
            @NonNull
            @Override
            public String newInstance(Object p1) {
                return String.valueOf(p1);
            }
        });
        return database.rawQuery(query, selectionArgs);
    }

    public final <T> ArrayList<T> toList(SQLiteDatabase database, Selector.SingleSelector<T> selector) {
        return Selector.toList(fetch(database), selector, allExpressible()[0]);
    }

    public final <T extends DependencyObject> List<T> toList(SQLiteDatabase database, Class<T> clazz) {
        return Selector.toList(fetch(database), clazz);
    }

    public final <T extends DependencyObject> T firstOrNull(SQLiteDatabase database, Class<T> clazz) {
        return Selector.firstOrNull(fetch(database), clazz);
    }

    public final boolean exist(SQLiteDatabase database) {
        return Selector.exist(fetch(database));
    }


}
