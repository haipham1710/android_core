package tmt.common.data.linq;

import android.support.annotation.NonNull;

import java.util.Date;

public final class LiteralExpression<T> extends Expression<T> {

    private final Object value;

    public LiteralExpression(Object value) {
        this.value = typeConvert(value);
    }

    public LiteralExpression(int value) {
        this.value = value;
    }

    public LiteralExpression(long value) {
        this.value = value;
    }

    public LiteralExpression(boolean value) {
        this.value = value ? 1 : 0;
    }

    private static Object typeConvert(Object value) {
        if (value instanceof Enum<?>) {
            return ((Enum) value).ordinal();
        }
        if (value instanceof Date) {
            return ((Date) value).getTime();
        }
        return value;
    }

    @NonNull
    @Override
    public String clause() {
        return "?";
    }

    @NonNull
    @Override
    public Object[] args() {
        return new Object[]{ value };
    }
}
