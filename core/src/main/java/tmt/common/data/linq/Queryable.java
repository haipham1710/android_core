package tmt.common.data.linq;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import tmt.common.Arrays;
import tmt.common.data.DependencyObject;
import tmt.common.data.sqlite.Table;
import tmt.common.presentation.DefaultConstructor;

public class Queryable extends Selectable {
    private final Selectable source;
    private final List<Expressible> whereClauses = new ArrayList<>();

    private Queryable(Selectable source) {
        this.source = source;
    }

    public static Queryable from(Class<? extends DependencyObject> clazz) {
        return new Queryable(Table.of(clazz));
    }

    public static Queryable from(Selectable source) {
        return new Queryable(source);
    }

    public Queryable where(Expressible expression) {
        whereClauses.add(expression);
        return this;
    }

    @Override
    public NamedExpressible[] allExpressible() {
        return source.allExpressible();
    }

    @NonNull
    @Override
    public String clause() {
        NamedExpressible[] selections = source.allExpressible();
        final String[] options = source.options();
        if (selections == null || selections.length == 0) {
            throw new IllegalStateException("No selection");
        }
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ");
        for (int i = 0; i < options.length; i++) {
            if (i != 0) {
                sb.append(", ");
            }
            sb.append(options[i]);
        }
        sb.append(" ");
        for (int i = 0; i < selections.length; i++) {
            if (i != 0) {
                sb.append(", ");
            }
            sb.append(selections[i].name());
        }
        sb.append(" FROM ").append(source.clause());
        appendWhereClause(sb, true);
        return sb.toString();
    }

    private StringBuilder appendWhereClause(StringBuilder sb, boolean prefixWHERE) {
        if (whereClauses.size() != 0) {
            if (prefixWHERE) {
                sb.append(" WHERE ");
            }
            for (int i = 0; i < whereClauses.size(); i++) {
                if (i != 0) {
                    sb.append(" AND ");
                }
                sb.append("(").append(whereClauses.get(i).clause()).append(")");
            }
        }
        return sb;
    }

    @NonNull
    @Override
    public Object[] args() {
        ArrayList<Object> args = new ArrayList<>();
        for (Expressible exp : whereClauses) {
            Collections.addAll(args, exp.args());
        }
        return args.toArray(new Object[args.size()]);
    }

    /**
     * extension method
     *
     * @param database Writable database
     */
    public void delete(SQLiteDatabase database) {
        Object[] args = args();
        String[] stringArgs = new String[args.length];
        Arrays.map(args, stringArgs, new DefaultConstructor.Constructor1<String, Object>() {
            @NonNull
            @Override
            public String newInstance(Object p1) {
                return p1.toString();
            }
        });
        database.delete(source.clause(), appendWhereClause(new StringBuilder(), false).toString(), stringArgs);
    }

    public void update(SQLiteDatabase database, @NonNull Setter<?>... setters) {
        Object[] args = args();
        String[] stringArgs = new String[args.length];
        Arrays.map(args, stringArgs, new DefaultConstructor.Constructor1<String, Object>() {
            @NonNull
            @Override
            public String newInstance(Object p1) {
                return p1.toString();
            }
        });
        ContentValues values = new ContentValues();
        for (Setter setter : setters) {
            Table.putToContentValues(values, setter.expression, setter.value);
        }
        database.update(source.clause(), values, appendWhereClause(new StringBuilder(), false).toString(), stringArgs);
    }
}
