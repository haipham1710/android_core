package tmt.common.data.linq;

import android.support.annotation.NonNull;

public interface NamedExpressible extends Expressible {

    @NonNull
    String name();

    @NonNull
    NamedExpressible desc();

    @NonNull
    NamedExpressible asc();

}
