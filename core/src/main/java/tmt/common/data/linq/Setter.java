package tmt.common.data.linq;

public class Setter<T> {

    public final NamedExpression<T> expression;
    public final T value;

    private Setter(NamedExpression<T> expression, T value) {
        this.expression = expression;
        this.value = value;
    }

    public static <T> Setter<T> of(NamedExpression<T> expression, T value) {
        return new Setter<>(expression, value);
    }
}
