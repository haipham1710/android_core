package tmt.common.data.linq;


import android.support.annotation.NonNull;

import tmt.common.Arrays;

public class BinaryExpression<T> extends Expression<T> {
    private final Operator operator;
    private final Expressible right;
    private final Expressible left;

    enum Operator {

        LESS_THAN("<"), LESS_THAN_OR_EQUALS("<="), EQUALS("="), GREATER_OR_EQUALS(">="), GREATER_THAN(">"), NOT_EQUALS("<>"),
        AND("AND"), OR("OR"),
        LIKE("LIKE");
        final String op;

        Operator(String op) {
            this.op = op;
        }
    }

    BinaryExpression(Operator operator, Expressible left, Expressible right) {
        this.operator = operator;
        this.left = left;
        this.right = right;
    }

    @NonNull
    @Override
    public String clause() {
        return "(" + left.clause() + ")" + operator.op + "(" + right.clause() + ")";
    }

    @NonNull
    @Override
    public Object[] args() {
        Object[] leftArgs = left.args();
        Object[] rightArgs = right.args();
        return Arrays.concat(new Object[leftArgs.length + rightArgs.length], leftArgs, rightArgs);
    }
}
