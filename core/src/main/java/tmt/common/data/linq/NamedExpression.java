package tmt.common.data.linq;

import android.support.annotation.NonNull;

public class NamedExpression<T> extends Expression<T> implements NamedExpressible {

    public final Expression<T> base;
    public final String name;

    public NamedExpression(final String name) {
        this(new Expression<T>() {
            @NonNull
            @Override
            public String clause() {
                return name;
            }

            @NonNull
            @Override
            public Object[] args() {
                return new Object[]{};
            }
        }, name);
    }

    public NamedExpression(Expression<T> base, String name) {
        this.base = base;
        this.name = name;
    }

    @NonNull
    @Override
    public String name() {
        return name;
    }

    @NonNull
    @Override
    public NamedExpressible desc() {
        return named(name + " DESC");
    }

    @NonNull
    @Override
    public NamedExpressible asc() {
        return named(name + " ASC");
    }

    public NamedExpression<T> named(String newName) {
        return new NamedExpression<>(this, newName);
    }

    @NonNull
    @Override
    public String clause() {
        return base.clause();
    }

    @NonNull
    @Override
    public Object[] args() {
        return base.args();
    }

    @Override
    public String toString() {
        return name;
    }
}
