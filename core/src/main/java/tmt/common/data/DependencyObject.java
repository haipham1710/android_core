package tmt.common.data;

import android.util.SparseArray;

public class DependencyObject {
    private final SparseArray<Object> values = new SparseArray<>();

    public final <T> T getValue(DependencyProperty<T> property) {
        @SuppressWarnings("unchecked")
        T value = (T) values.get(property.id);
        if (value == null) {
            value = property.defaultValue;
        }
        return value;
    }

    public final <T> void setValue(DependencyProperty<T> property, T value) {
        values.put(property.id, value);
    }
}
