package tmt.common.data;

import java.util.HashMap;
import java.util.Map;

import tmt.common.data.linq.NamedExpression;

public class DependencyProperty<Type> extends NamedExpression<Type> {

    final int id;
    final Type defaultValue;
    private static int __id;
    private static final HashMap<Class<? extends DependencyObject>, HashMap<String, DependencyProperty>> properties = new HashMap<>();


    protected DependencyProperty(String name, Class<? extends DependencyObject> owner, Class<Type> type, Type defaultValue) {
        super(name);
        HashMap<String, DependencyProperty> maps = properties.get(owner);
        if (maps == null) {
            maps = new HashMap<>();
            maps.put(name, this);
            properties.put(owner, maps);
        } else {
            if (maps.containsKey(name)) {
                throw new IllegalStateException("Duplicated key " + name + " of class " + owner.getName());
            }
            maps.put(name, this);
        }
        if (defaultValue == null) {
            this.defaultValue = getDefaultValueOfType(type);
        } else {
            this.defaultValue = defaultValue;
        }
        this.id = nextId();
    }

    private static <Type> Type getDefaultValueOfType(Class<Type> type) {
        Object def;
        if (type == Boolean.class || type == Boolean.TYPE) {
            def = false;
        } else if (type == Integer.class || type == Integer.TYPE) {
            def = 0;
        } else if (type == Double.class || type == Double.TYPE) {
            def = 0d;
        } else if (type == Long.class || type == Long.TYPE) {
            def = 0L;
        } else if (type == Float.class || type == Float.TYPE) {
            def = (float) 0;
        } else {
            def = null;
        }
        @SuppressWarnings("unchecked") Type typedDef = (Type) def;
        return typedDef;
    }

    private static synchronized int nextId() {
        return __id++;
    }

    public static DependencyProperty[] collect(Class<? extends DependencyObject> clazz) {
        HashMap<String, DependencyProperty> maps = properties.get(clazz);
        if (maps == null) {
            return null;
        } else {
            DependencyProperty[] properties = new DependencyProperty[maps.size()];
            int i = 0;
            for (Map.Entry<String, DependencyProperty> pair : maps.entrySet()) {
                properties[i++] = pair.getValue();
            }
            return properties;
        }
    }

    public static <T> DependencyProperty<T> register(String name, Class<? extends DependencyObject> owner, Class<T> type) {
        return new DependencyProperty<>(name, owner, type, null);
    }

    public static <T> DependencyProperty<T> register(String name, Class<? extends DependencyObject> owner, Class<T> type, T defaultValue) {
        return new DependencyProperty<>(name, owner, type, defaultValue);
    }
}
