package tmt.common.data.sqlite;

import android.database.Cursor;

import java.util.Date;

import tmt.common.data.DependencyObject;
import tmt.common.data.DependencyProperty;

public class Column<Type> extends DependencyProperty<Type> {
    private interface CursorGetter<Type> {
        Type get(Cursor cursor, int columnIndex);
    }

    private final boolean noneNull;
    boolean isPrimaryKey;
    ForeignKey<Type> foreignKey;
    final SqliteType type;
    private final CursorGetter<Type> cursorGetter;

    protected Column(String name, Class<? extends DependencyObject> owner, Class<Type> type, boolean noneNull, Type defaultValue) {
        super(name, owner, type, defaultValue);
        this.noneNull = noneNull;
        this.type = getSqlTypeOf(type);
        this.cursorGetter = getCursorGetterOf(type);
    }

    public boolean isAllowNull() {
        return !isPrimaryKey && !noneNull;
    }

    public boolean isAutoincrement() {
        return type == SqliteType.INTEGER && isPrimaryKey;
    }

    @SuppressWarnings({ "unchecked" })
    private static <T> CursorGetter<T> getCursorGetterOf(Class<? extends T> type) {
        if (type == String.class) {
            return (CursorGetter<T>) stringGetter;
        }
        if (type == Integer.class || type == Integer.TYPE) {
            return (CursorGetter<T>) intGetter;
        }
        if (type == Date.class) {
            return (CursorGetter<T>) dateGetter;
        }
        if (type == Boolean.class || type == Boolean.TYPE) {
            return (CursorGetter<T>) booleanGetter;
        }
        if (Enum.class.isAssignableFrom(type)) {
            return (CursorGetter<T>) new EnumGetter<>(type);
        }
        if (Double.class == type || type == Double.TYPE) {
            return (CursorGetter<T>) doubleGetter;
        }
        if (Float.class == type || type == Float.TYPE) {
            return (CursorGetter<T>) floatGetter;
        }
        if (Long.class == type || type == Long.TYPE) {
            return (CursorGetter<T>) longGetter;
        }
        throw new UnsupportedOperationException("Unsupported type " + type.getName() + " in cursor");
    }

    public Type getValue(Cursor cursor, int columnIndex) {
        return cursorGetter.get(cursor, columnIndex);
    }

    private static SqliteType getSqlTypeOf(Class<?> type) {
        if (type == String.class) {
            return SqliteType.TEXT;
        }
        if (Integer.class == type || Long.class == type || Boolean.class == type || Enum.class.isAssignableFrom(type) || Date.class == type ||
                Integer.TYPE == type || Long.TYPE == type || Boolean.TYPE == type) {
            return SqliteType.INTEGER;
        }
        if (Double.class == type || Float.class == type ||
                Double.TYPE == type || Float.TYPE == type) {
            return SqliteType.REAL;
        }
        return SqliteType.NULL;
    }

    public static <T> Column<T> register(String name, Class<? extends DependencyObject> owner, Class<T> type) {
        return new Column<>(name, owner, type, false, null);
    }

    public static <T> Column<T> register(String name, Class<? extends DependencyObject> owner, Class<T> type, T defaultValue) {
        return new Column<>(name, owner, type, false, defaultValue);
    }

    public static <T> Column<T> register(String name, Class<? extends DependencyObject> owner, Class<T> type, boolean noneNull) {
        return new Column<>(name, owner, type, noneNull, null);
    }

    public static <T> Column<T> register(String name, Class<? extends DependencyObject> owner, Class<T> type, boolean noneNull, T defaultValue) {
        return new Column<>(name, owner, type, noneNull, defaultValue);
    }

    public final Column<Type> isPrimaryKey() {
        isPrimaryKey = true;
        return this;
    }

    public final <X extends DependencyObject> Column<Type> isForeignKey(Class<X> other, Column<Type> column) {
        foreignKey = new ForeignKey<>(other, column);
        return this;
    }

    private static final CursorGetter<String> stringGetter = new CursorGetter<String>() {

        @Override
        public String get(Cursor cursor, int columnIndex) {
            return cursor.getString(columnIndex);
        }
    };

    private static final CursorGetter<Double> doubleGetter = new CursorGetter<Double>() {

        @Override
        public Double get(Cursor cursor, int columnIndex) {
            return cursor.getDouble(columnIndex);
        }
    };

    private static final CursorGetter<Integer> intGetter = new CursorGetter<Integer>() {

        @Override
        public Integer get(Cursor cursor, int columnIndex) {
            return cursor.getInt(columnIndex);
        }
    };

    private static final CursorGetter<Long> longGetter = new CursorGetter<Long>() {

        @Override
        public Long get(Cursor cursor, int columnIndex) {
            return cursor.getLong(columnIndex);
        }
    };

    private static final CursorGetter<Float> floatGetter = new CursorGetter<Float>() {

        @Override
        public Float get(Cursor cursor, int columnIndex) {
            return cursor.getFloat(columnIndex);
        }
    };
    private static final CursorGetter<Boolean> booleanGetter = new CursorGetter<Boolean>() {

        @Override
        public Boolean get(Cursor cursor, int columnIndex) {
            return cursor.getInt(columnIndex) != 0;
        }
    };
    private static final CursorGetter<Date> dateGetter = new CursorGetter<Date>() {

        @Override
        public Date get(Cursor cursor, int columnIndex) {
            long value = cursor.getLong(columnIndex);
            return value <= 0 ? null : new Date(value);
        }
    };

    private static final class EnumGetter<X> implements CursorGetter<X> {
        private final Class<X> clazz;

        private EnumGetter(Class<X> clazz) {
            this.clazz = clazz;
        }

        @Override
        public X get(Cursor cursor, int columnIndex) {
            return clazz.getEnumConstants()[cursor.getInt(columnIndex)];
        }
    }
}
