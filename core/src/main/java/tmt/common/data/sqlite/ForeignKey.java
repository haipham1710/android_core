package tmt.common.data.sqlite;

import tmt.common.data.DependencyObject;

class ForeignKey<T> {
    public final Class<? extends DependencyObject> otherTable;
    public final Column<T> otherColumn;
    public final String otherTableName;

    public ForeignKey(Class<? extends DependencyObject> otherTable, Column<T> otherColumn) {
        this.otherColumn = otherColumn;
        this.otherTable = otherTable;
        otherTableName = Table.nameOf(otherTable);
    }
}
