package tmt.common.data.sqlite;

import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

import tmt.common.data.DependencyObject;
import tmt.common.data.linq.NamedExpressible;

public final class Selector {

    public static <T extends DependencyObject> List<T> toList(Cursor cursor, Class<T> clazz) {
        ArrayList<T> result = new ArrayList<>();
        final Table table = Table.of(clazz);
        if (cursor.moveToFirst()) {
            final Column[] columns = table.allExpressible();
            int[] columnsIndex = new int[columns.length];
            for (int i = 0; i < columnsIndex.length; i++) {
                columnsIndex[i] = cursor.getColumnIndex(columns[i].name());
            }
            do {

                T object;
                try {
                    object = clazz.newInstance();
                } catch (InstantiationException e) {
                    throw new RuntimeException(clazz.getName() + " doesn't have default constructor", e);
                } catch (IllegalAccessException e) {
                    throw new RuntimeException(clazz.getName() + " is inaccessible", e);
                }
                for (int i = 0; i < columnsIndex.length; i++) {
                    object.setValue(columns[i], columns[i].getValue(cursor, columnsIndex[i]));
                }
                result.add(object);
            } while (cursor.moveToNext());
        }
        return result;
    }

    public static boolean exist(Cursor cursor) {
        try {
            return cursor.moveToFirst();
        } finally {
            cursor.close();
        }
    }

    public interface SingleSelector<T> {
        T select(Cursor cursor, int columnIndex);
    }

    public static <T extends DependencyObject> T firstOrNull(Cursor cursor, Class<T> clazz) {
        if (cursor.moveToFirst()) {
            T value;
            try {
                value = clazz.newInstance();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }

            for (Column col : Table.of(clazz).allExpressible()) {
                value.setValue(col, col.getValue(cursor, cursor.getColumnIndex(col.name)));
            }
            return value;
        }
        return null;
    }

    public static <T> ArrayList<T> toList(Cursor cursor, SingleSelector<T> selector, NamedExpressible column) {
        ArrayList<T> result = new ArrayList<>();

        if (cursor.moveToFirst()) {
            int columnIndex = cursor.getColumnIndex(column.name());
            do {
                T value = selector.select(cursor, columnIndex);
                result.add(value);
            } while (cursor.moveToNext());
        }
        return result;
    }

}
