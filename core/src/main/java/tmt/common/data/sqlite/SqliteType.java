package tmt.common.data.sqlite;

import android.database.Cursor;

public enum SqliteType {
    INTEGER("INTEGER", Cursor.FIELD_TYPE_INTEGER),
    REAL("REAL", Cursor.FIELD_TYPE_FLOAT),
    TEXT("TEXT", Cursor.FIELD_TYPE_STRING),
    BLOB("BLOB", Cursor.FIELD_TYPE_BLOB),
    NULL("NULL", Cursor.FIELD_TYPE_NULL);

    public final int fieldType;
    public final String name;

    SqliteType(String name, int type) {
        this.name = name;
        this.fieldType = type;
    }
}
