package tmt.common.data.sqlite;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import tmt.common.Arrays;
import tmt.common.data.DependencyObject;
import tmt.common.data.DependencyProperty;
import tmt.common.data.Named;
import tmt.common.data.linq.NamedExpressible;
import tmt.common.data.linq.Selectable;
import tmt.common.presentation.Predicate;

public class Table extends Selectable {
    private static final HashMap<Class, Table> cached = new HashMap<>();

    private final Column[] columns;

    private final String name;

    private Table(String name, Column<?>[] columns) {
        this.columns = columns;
        this.name = name;
    }

    public static String nameOf(Class<? extends DependencyObject> clazz) {
        final Named annotation = clazz.getAnnotation(Named.class);
        return annotation == null ? clazz.getSimpleName() : annotation.value();
    }

    public static <X extends DependencyObject> Table of(Class<X> clazz) {
        forceClassInitialize(clazz);
        Table table = cached.get(clazz);
        if (table != null) {
            return table;
        }
        DependencyProperty[] properties = DependencyProperty.collect(clazz);
        if (properties == null) {
            throw new IllegalStateException(clazz.getName() + " doesn't have any column");
        }
        ArrayList<Column> columns = new ArrayList<>();
        for (DependencyProperty property : properties) {
            if (property instanceof Column) {
                columns.add((Column) property);
            }
        }
        if (columns.isEmpty()) {
            throw new IllegalStateException(clazz.getName() + "doesn't have any columns");
        }
        table = new Table(nameOf(clazz), columns.toArray(new Column[columns.size()]));
        cached.put(clazz, table);
        return table;
    }

    private static void forceClassInitialize(Class<?> clazz) {
        try {
            Class.forName(clazz.getName());
        } catch (ClassNotFoundException ignored) {
        }
    }

    @Override
    public Column[] allExpressible() {
        return columns;
    }

    @NonNull
    @Override
    public String clause() {
        return name;
    }

    @NonNull
    @Override
    public Object[] args() {
        return new Object[0];
    }

    @NonNull
    public final String buildCreateStatement() {
        StringBuilder sb = new StringBuilder();
        sb.append("CREATE TABLE ").append(name).append(" (");
        boolean first = true;
        for (Column column : columns) {
            if (first) {
                first = false;
            } else {
                sb.append(",");
            }
            sb.append("\r\n");
            sb.append(column.name).append(" ").append(column.type.name);
            if (!column.isAllowNull() || column.isPrimaryKey) {
                sb.append(" NOT NULL");
            }
        }
        final ArrayList<Column> primaries = getPrimaries();
        if (primaries.size() != 0) {
            sb.append(",\r\n PRIMARY KEY (");
            boolean f = true;
            for (Column primary : primaries) {
                if (f) {
                    f = false;
                } else {
                    sb.append(",");
                }
                sb.append(primary.name);
            }
            sb.append(")");
        }
        final ArrayList<InternalForeignKey> foreignKeys = getForeignKeys();
        for (InternalForeignKey foreign : foreignKeys) {
            sb.append(",\r\nFOREIGN KEY (");
            boolean f = true;
            for (Column column : foreign.thisKeys) {
                if (f) {
                    f = false;
                } else {
                    sb.append(",");
                }
                sb.append(column.name);
            }
            sb.append(") REFERENCES ").append(foreign.foreignTable.name).append("(");
            f = true;
            for (Column column : foreign.foreignKeys) {
                if (f) {
                    f = false;
                } else {
                    sb.append(",");
                }
                sb.append(column.name);
            }
            sb.append(")");
        }
        sb.append(")");
        return sb.toString();
    }

    public String buildClearStatements() {
        return "DELETE FROM " + name;
    }

    private ArrayList<Column> getPrimaries() {
        ArrayList<Column> properties = new ArrayList<>();
        for (Column property : columns) {
            if (property.isPrimaryKey) {
                properties.add(property);
            }
        }
        return properties;
    }

    private ArrayList<InternalForeignKey> getForeignKeys() {
        ArrayList<Column> properties = new ArrayList<>();
        for (Column property : columns) {
            if (property.foreignKey != null) {
                properties.add(property);
            }
        }
        ArrayList<InternalForeignKey> internalForeignKeys = new ArrayList<>();
        for (Column col : properties) {
            final String name = col.foreignKey.otherTableName;
            InternalForeignKey key = Arrays.first(internalForeignKeys, new Predicate<InternalForeignKey>() {
                @Override
                public boolean value(InternalForeignKey x) {
                    return x.foreignTable.name.equals(name);
                }
            });
            if (key == null) {
                final Table foreignTable = Table.of(col.foreignKey.otherTable);
                if (foreignTable == null) {
                    throw new IllegalStateException("Not found table " + col.foreignKey.otherTableName + " to reference foreign key");
                }
                key = new InternalForeignKey(foreignTable);
                key.thisKeys.add(col);
                key.foreignKeys.add(col.foreignKey.otherColumn);
                internalForeignKeys.add(key);
            } else {
                key.thisKeys.add(col);
                key.foreignKeys.add(col.foreignKey.otherColumn);
            }
        }
        return internalForeignKeys;
    }

    public final ContentValues toContentValues(ContentValues values, DependencyObject object) {
        if (values == null) {
            values = new ContentValues();
        }
        Object o;
        for (Column<?> column : columns) {
            if (column.isAutoincrement()) {
                continue;
            }
            o = object.getValue(column);
            putToContentValues(values, column, o);
        }
        return values;
    }

    public static void putToContentValues(ContentValues values, NamedExpressible column, Object o) {
        if (o == null) {
            values.put(column.name(), (String) null);
        } else if (o instanceof String) {
            values.put(column.name(), (String) o);
        } else if (o instanceof Integer) {
            values.put(column.name(), (Integer) o);
        } else if (o instanceof Enum<?>) {
            values.put(column.name(), ((Enum<?>) o).ordinal());
        } else if (o instanceof Double) {
            values.put(column.name(), (Double) o);
        } else if (o instanceof Long) {
            values.put(column.name(), (Long) o);
        } else if (o instanceof Float) {
            values.put(column.name(), (Float) o);
        } else if (o instanceof Byte) {
            values.put(column.name(), (Byte) o);
        } else if (o instanceof Short) {
            values.put(column.name(), (Short) o);
        } else if (o instanceof Boolean) {
            values.put(column.name(), (Boolean) o);
        } else if (o instanceof byte[]) {
            values.put(column.name(), (byte[]) o);
        } else if (o instanceof Date) {
            values.put(column.name(), ((Date) o).getTime());
        } else {
            throw new UnsupportedOperationException("Unsupported data type " + o.getClass().getName());
        }
    }

    @Override
    public String toString() {
        return name;
    }

    public void insert(SQLiteDatabase database, DependencyObject entity) {
        database.beginTransaction();
        try {
            database.insert(name,
                    null, // nullColumnHack
                    toContentValues(null, entity));
            database.setTransactionSuccessful();
        } finally {
            database.endTransaction();
        }
    }

    private static class InternalForeignKey {
        public final ArrayList<Column<?>> thisKeys = new ArrayList<>();
        public final Table foreignTable;
        public final ArrayList<Column<?>> foreignKeys = new ArrayList<>();

        public InternalForeignKey(Table foreignTable) {
            this.foreignTable = foreignTable;
        }
    }
}
