package tmt.common;

import android.support.annotation.Nullable;
import android.text.TextUtils;

public class StringHelper {
    public static final String EMPTY_UUID = "00000000-0000-0000-0000-000000000000";
    private static final char[][] map =
            { { '\n', '\u2FC2' }, { '\r', '\u2FC4', }, { ',', '\u2FC3' } };

    public static String encode(String input) {
        if (input != null && input.length() != 0) {
            char[] chars = input.toCharArray();
            for (int i = 0; i < chars.length; i++) {
                for (char[] e : map) {
                    if (chars[i] == e[0]) {
                        chars[i] = e[1];
                    }
                }
            }
            return new String(chars);
        }
        return input;
    }

    public static String decode(String input) {
        if (input != null && input.length() != 0) {
            char[] chars = input.toCharArray();
            for (int i = 0; i < chars.length; i++) {
                for (char[] e : map) {
                    if (chars[i] == e[1]) {
                        chars[i] = e[0];
                    }
                }
            }
            return new String(chars);
        }
        return input;
    }

    public static String stripHtml(String input) {
        if (input == null || input.length() == 0) {
            return input;
        }
        input = input.replaceAll("</?\\w+((\\s+\\w+(\\s*=\\s*(?:\".*?\"|'.*?'|[^'\">\\s]+))?)+\\s*|\\s*)/?>", "");
        input = input.replaceAll("\\[[^]]+\\]", "");
        return input;
    }

    public static String removeNewLine(String value, int maxLength) {
        if (value == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        char charAt;
        boolean alreadyAppendSpace = false;
        int len = value.length() < maxLength ? value.length() : maxLength;
        for (int i = 0; i < len; i++) {
            charAt = value.charAt(i);
            if (charAt == '\r' || charAt == '\n') {
                if (!alreadyAppendSpace) {
                    sb.append(' ');
                    alreadyAppendSpace = true;
                } else {
                    alreadyAppendSpace = false;
                }
            } else {
                sb.append(charAt);
            }
        }
        return sb.toString();
    }

    public static String trimAll(String input) {
        if (TextUtils.isEmpty(input)) {
            return input;
        }
        StringBuilder sb = new StringBuilder(input.length());
        boolean hasSpace = true;
        char c;
        for (int i = 0; i < input.length(); i++) {
            c = input.charAt(i);
            switch (c) {
                case '\r':
                case ' ':
                case '\n':
                case '\t':
                case '\u00A0':
                    if (!hasSpace) {
                        sb.append(' ');
                        hasSpace = true;
                    }
                    break;
                default:
                    hasSpace = false;
                    sb.append(c);
                    break;
            }
        }
        return sb.toString().trim();
    }

    public static String maxLength(String input, int len) {
        if (input == null || input.length() <= len) {
            return input;
        }
        return input.substring(0, len);
    }

    public static boolean equals(Object a, Object b) {
        return a == b || (a != null && a.equals(b));
    }

    public static String fileNameOfPath(String path) {
        if (TextUtils.isEmpty(path)) {
            return path;
        }
        int i = path.lastIndexOf('/');
        if (i >= 0) {
            return path.substring(i + 1);
        }
        return path;
    }

    public static String stringOf(@Nullable Object value) {
        String s = value != null ? value.toString() : null;
        return s == null ? "" : s;
    }
}
