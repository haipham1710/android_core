package tmt.common.presentation;

import android.text.Editable;
import android.text.TextWatcher;

import tmt.common.StringHelper;

public abstract class DefaultTextWatcher implements TextWatcher {
    private String before;

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    protected abstract void onTextChanged(String s);

    @Override
    public final void afterTextChanged(Editable s) {
        String newValue = s.toString();
        if (!StringHelper.equals(newValue, before)) {
            this.before = newValue;
            onTextChanged(newValue);
        }
    }

}
