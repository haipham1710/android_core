package tmt.common.presentation;

public interface Property<T, V> {

    V get(T object);

    void set(T object, V value);

}
