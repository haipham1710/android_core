package tmt.common.presentation;

public abstract class Activator<HostType> implements DefaultConstructor<HostType> {
    public abstract HostType newInstance();

    public abstract HostType[] newArray(int size);

    public HostType createFromException(Exception e) {
        return null;
    }

    public static abstract class Activator1<HostType, T1> implements Constructor1<HostType, T1> {
        public abstract HostType[] newArray(int size);
    }
}