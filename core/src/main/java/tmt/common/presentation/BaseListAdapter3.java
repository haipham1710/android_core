package tmt.common.presentation;

import android.content.Context;
import android.text.TextUtils;

/***
 * BaseListAdapter2 that supports async keyword
 *
 * @param <CollectionType>
 * @author Starfall
 */
public abstract class BaseListAdapter3<CollectionType> extends BaseListAdapter2<CollectionType> {

    public BaseListAdapter3(Context context, CollectionType empty, CollectionType[] items) {
        super(context, empty, items);
    }

    public BaseListAdapter3(Context context, CollectionType[] items) {
        super(context, items);
    }

    public BaseListAdapter3(Context context, Iterable<CollectionType> items) {
        super(context, items);
    }

    public BaseListAdapter3(Context context) {
        super(context);
    }

    protected String keyword = null;
    private String pendingKeyword = null;
    private boolean hasPendingKeyword = false;

    @Override
    public void reset(String keyword) {
        if (isFetching()) {
            pendingKeyword = keyword;
            hasPendingKeyword = true;
        } else {
            super.reset();
            this.keyword = keyword;
            fetch();
        }
    }

    @Override
    protected void onCompleteFetch() {
        super.onCompleteFetch();
        if (hasPendingKeyword) {
            keyword = pendingKeyword;
            pendingKeyword = null;
            hasPendingKeyword = false;
            reset();
            fetch();
        }
    }

    @Override
    public boolean hasKeyword() {
        return !TextUtils.isEmpty(keyword);
    }
}
