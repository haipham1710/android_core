package tmt.common.presentation;

public enum ScreenOrientation {
    Landscape,
    Portrait,
    FlippedLandscape,
    FlippedPortrait;

    public final boolean isLandscape() {
        return this == Landscape || this == FlippedLandscape;
    }
}
