package tmt.common.presentation;

import android.support.annotation.NonNull;

public interface DefaultConstructor<HostType> {
    HostType newInstance();

    interface Constructor1<HostType, T1> {
        @NonNull
        HostType newInstance(T1 p1);

    }

    interface Constructor2<HostType, T1, T2> {
        HostType newInstance(T1 p1, T2 p2);

    }

    interface Constructor3<HostType, T1, T2, T3> {
        HostType newInstance(T1 p1, T2 p2, T3 p3);

    }

}
