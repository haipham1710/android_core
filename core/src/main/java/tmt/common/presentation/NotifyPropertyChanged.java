package tmt.common.presentation;

import java.lang.ref.WeakReference;

public abstract class NotifyPropertyChanged {
    public interface OnPropertyChangedListener {
        void onPropertyChanged(Object sender);
    }

    private WeakReference<OnPropertyChangedListener> target;

    public final void registerPropertyChanged(OnPropertyChangedListener listener) {
        target = new WeakReference<>(listener);
    }

    public final void notifyPropertyChanged() {
        OnPropertyChangedListener listener = target == null ? null : target.get();
        if (listener != null) {
            listener.onPropertyChanged(this);
        }
    }
}
