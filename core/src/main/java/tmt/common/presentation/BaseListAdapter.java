package tmt.common.presentation;

import android.content.Context;
import android.os.Parcelable;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.net.UnknownHostException;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import tmt.common.ArrayHelper;
import tmt.common.presentation.DefaultConstructor.Constructor1;
import tmt.common.threading.Task;
import tmt.common.threading.TaskCompletion;
import tmt.common.threading.TaskSource;
import tmt.common.util.SavedInstanceStateData;

/**
 * BaseAdapter that supports Fetchable
 *
 * @param <CollectionType>
 * @author Starfall
 */
public abstract class BaseListAdapter<CollectionType> extends BaseAdapter
        implements Iterable<CollectionType>, FetchableAdapter {
    protected static final int VIEW_TYPE_EMPTY = 1;
    protected static final int VIEW_TYPE_ITEM = 0;

    private boolean canNext = true;
    private boolean fetching = false;
    private int page = 0;
    private boolean isEnableEmptyRow;
    private int viewTypeCount = 2;

    public boolean isEnableEmptyRow() {
        return isEnableEmptyRow;
    }

    private final ArrayList<CollectionType> items = new ArrayList<>();

    private LayoutInflater layoutInflater;
    private Context context;
    private FetchingListener fetchingListener;
    private CharSequence emptyText;
    private CharSequence loadingText;
    private boolean pendingCleared;
    private Exception currentException;
    private int emptyLayoutId;

    public BaseListAdapter() {

    }

    public BaseListAdapter(Context context) {
        this(context, null, (Iterable<CollectionType>) null);
    }

    public BaseListAdapter(Context context, CollectionType empty, CollectionType[] items) {
        this(context, empty, ArrayHelper.toIterable(items));
    }

    public BaseListAdapter(Context context, CollectionType[] items) {
        this(context, ArrayHelper.toIterable(items));
    }

    public BaseListAdapter(Context context, CollectionType empty, Iterable<CollectionType> items) {
        setContext(context);
        if (empty != null) {
            this.items.add(empty);
        }
        if (items != null) {
            pendingCleared = true;
            for (CollectionType item : items) {
                this.items.add(item);
            }
        }
    }

    public void setContext(Context context) {
        this.context = context;
        layoutInflater = context == null ? null : LayoutInflater.from(context);
    }

    public BaseListAdapter(Context context, Iterable<CollectionType> items) {
        this(context, null, items);
    }

    protected final int registerViewType() {
        return viewTypeCount++;
    }

    public void add(CollectionType item) {
        if (item == null) {
            throw new InvalidParameterException("item is added to collection cannot be null");
        }
        items.add(item);
    }

    public void clear() {
        canNext = true;
        page = 0;
        items.clear();
    }

    @Override
    public void fetch() {
        if (!canNext || fetching) {
            return;
        }
        fetching = true;
        if (fetchingListener != null) {
            fetchingListener.onBeginFetch();
        }
        realizeItems(++page).then(new TaskCompletion<List<CollectionType>>() {

            @Override
            public void onContinue(Task<List<CollectionType>> task) {
                boolean shouldNotify = fetching;
                boolean hasValue = false;
                fetching = false;

                if (task.isFaulted()) {
                    currentException = task.getException();
                    onHasException(currentException);
                } else if (task.isCompleted()) {
                    if (pendingCleared) {
                        clear();
                        page = 1;
                    }

                    hasValue = task.getResult() != null && !task.getResult().isEmpty();

                    if (hasValue) {
                        addAll(task.getResult());
                    }
                    if (shouldNotify && (hasValue || pendingCleared)) {
                        notifyDataSetChanged();
                    }
                }
                canNext = canNext && hasValue;
                pendingCleared = false;
                if (shouldNotify) {
                    onCompleteFetch();
                }
            }
        });
    }

    @NonNull
    protected Task<List<CollectionType>> realizeItems(int page) {
        return TaskSource.cancelledTask();
    }

    public CollectionType findItem(CollectionType item) {
        for (CollectionType element : items) {
            if (element.equals(item)) {
                return element;
            }
        }
        return null;
    }

    protected final Context getContext() {
        return context;
    }

    @Override
    public int getCount() {
        if (isEnableEmptyRow && isEmpty()) {
            return 1;
        }
        return items.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (isEnableEmptyRow && isEmpty()) {
            return VIEW_TYPE_EMPTY;
        }
        return VIEW_TYPE_ITEM;
    }

    @Override
    public final int getViewTypeCount() {
        return viewTypeCount;
    }

    @Override
    public CollectionType getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return (long) position;
    }

    public int getItemIndex(CollectionType item) {
        int i = 0;
        for (CollectionType e : items) {
            if (e.equals(item)) {
                return i;
            }
            i += 1;
        }
        return -1;
    }

    @Override
    public abstract View getView(int position, View convertView, ViewGroup parent);

    public final void addAll(CollectionType[] data) {
        for (CollectionType e : data) {
            add(e);
        }
    }

    protected void onCompleteFetch() {
        if (fetchingListener != null) {
            fetchingListener.onCompleteFetch();
        }
    }

    protected void onHasException(Exception ex) {
        if (fetchingListener != null) {
            fetchingListener.hasException(ex);
        }
    }

    public final void addAll(Iterable<CollectionType> data) {
        for (CollectionType e : data) {
            add(e);
        }
    }

    protected final LayoutInflater inflater() {
        return layoutInflater;
    }

    @Override
    public Iterator<CollectionType> iterator() {
        return items.iterator();
    }

    public final CollectionType remove(int position) {
        return items.remove(position);
    }

    public final boolean remove(CollectionType item) {
        return items.remove(item);
    }

    public <T extends Comparable<T>> void sortDescending(Sortable<CollectionType, T> x) {
        Collections.sort(items, Sortable.Helper.toComparator(x));
    }

    public void reset() {
        clear();
        notifyDataSetChanged();
        fetch();
    }

    public void reset(String keyword) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean refresh() {
        if (fetching) {
            return false;
        }
        canNext = true;
        page = 0;
        pendingCleared = true;
        fetch();
        return true;
    }

    protected static String getEmptyTextByException(Exception exception) {
        if (exception == null) {
            return null;
        }
        if (exception instanceof UnknownHostException) {
            return "Mất kết nối";
        }
        return exception.getMessage();
    }

    public String getErrorMessage() {
        return getEmptyTextByException(currentException);
    }

    public boolean isEmpty() {
        return items.size() == 0;
    }

    @Override
    public void setFetchingListener(FetchingListener listener) {
        fetchingListener = listener;
    }

    protected final ArrayList<CollectionType> getItems() {
        return items;
    }

    public void insertFirst(CollectionType item) {
        items.add(0, item);
    }

    public final void add(int position, CollectionType item) {
        items.add(position, item);
    }

    protected void insertAfter(CollectionType item, CollectionType newItem) {
        items.add(items.indexOf(item) + 1, newItem);
    }

    protected int enableEmptyRow(int layoutResId, CharSequence loadingText, CharSequence emptyText) {
        this.isEnableEmptyRow = true;
        this.emptyText = emptyText;
        this.loadingText = loadingText;
        this.emptyLayoutId = layoutResId;
        return VIEW_TYPE_EMPTY;
    }

    protected View getEmptyRow(View convertView, ViewGroup parent) {
        View view = convertView == null ? inflater().inflate(emptyLayoutId, parent, false) : convertView;
        ((TextView) view.findViewById(android.R.id.empty)).setText(fetching ? loadingText : currentException == null ? emptyText : getEmptyTextByException(currentException));
        return view;
    }

    @NonNull
    public final SavedInstanceStateData saveState() {
        SavedInstanceStateData data = new SavedInstanceStateData();
        writeState(data);
        return data;
    }

    public final void restoreState(SavedInstanceStateData savedState) {
        readFromSavedState(savedState);
    }

    @CallSuper
    protected void readFromSavedState(SavedInstanceStateData source) {
        ArrayList<? extends Parcelable> savedItems = source.readObjectArrayList();
        if (savedItems != null && savedItems.size() != 0) {
            items.clear();
            for (Parcelable t : savedItems) {
                items.add((CollectionType) t);
            }
        }
        canNext = source.readBoolean();
        page = source.readInt();
        pendingCleared = source.readBoolean();
    }

    @CallSuper
    protected void writeState(SavedInstanceStateData dest) {
        dest.writeObjectArrayList((ArrayList<? extends Parcelable>) items);
        dest.writeBoolean(canNext);
        dest.writeInt(page);
        dest.writeBoolean(pendingCleared);
    }

    @Override
    public boolean isFetching() {
        return fetching;
    }

    public boolean hasKeyword() {
        return false;
    }

    public int size() {
        return items.size();
    }

    public CollectionType[] toArray(CollectionType[] destination) {
        int i = 0;
        for (CollectionType item : items) {
            destination[i++] = item;
        }
        return destination;
    }

    public <X> X[] toArray(Activator<X> activator, Constructor1<X, CollectionType> constructor) {
        return ArrayHelper.toArray(items, activator, constructor);
    }

    public ArrayList<CollectionType> toList() {
        ArrayList<CollectionType> list = new ArrayList<>();
        for (CollectionType item : items) {
            list.add(item);
        }
        return list;
    }
}
