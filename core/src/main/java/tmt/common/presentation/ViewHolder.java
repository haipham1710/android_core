package tmt.common.presentation;

import android.view.View;

public abstract class ViewHolder extends BaseViewHolder {

    public ViewHolder(View view) {
        super(view);
    }

    Object item;

    public void setItem(Object item) {
        this.item = item;
    }

    public Object item() {
        return item;
    }

    public void bind(int position) {

    }

}
