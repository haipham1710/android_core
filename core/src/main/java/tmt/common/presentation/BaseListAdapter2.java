package tmt.common.presentation;

import android.content.Context;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;

/**
 * BaseListAdapter that uses ViewHolder pattern
 *
 * @param <CollectionType>
 * @author Starfall
 */
public abstract class BaseListAdapter2<CollectionType> extends BaseListAdapter<CollectionType> {

    public enum LoadMode {
        None, Bottom, Top
    }

    private LoadMode loadMode = LoadMode.None;
    protected int itemRemainingToLoad = 5;

    public BaseListAdapter2() {

    }

    public BaseListAdapter2(Context context, Iterable<CollectionType> items) {
        super(context, items);
    }

    public BaseListAdapter2(Context context, CollectionType[] items) {
        super(context, items);
    }

    public BaseListAdapter2(Context context, CollectionType empty, CollectionType[] items) {
        super(context, empty, items);
    }

    public BaseListAdapter2(Context context) {
        super(context);
    }

    protected void setLoadMode(LoadMode loadMode) {
        this.loadMode = loadMode;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (isEmpty()) {
            return getEmptyRow(convertView, parent);
        }
        ViewHolder view;
        if (convertView == null) {
            view = onCreateViewHolder(parent, getItemViewType(position));
            onFirstBindViewHolder(view, position);
        } else {
            view = (ViewHolder) convertView.getTag();
            if (view == null) {
                throw new IllegalStateException("View holder is null in tag");
            }
        }
        onBindViewHolder(view, position);
        if (loadMode == LoadMode.Bottom) {
            if (position == size() - itemRemainingToLoad) {
                fetch();
            }
        } else if (loadMode == LoadMode.Top) {
            if (position == itemRemainingToLoad) {
                fetch();
            }
        }
        return view.view;
    }

    @NonNull
    public abstract ViewHolder onCreateViewHolder(ViewGroup parent, int viewType);

    protected void onFirstBindViewHolder(ViewHolder viewHolder, int position) {
    }

    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        Object item = getItem(position);
        viewHolder.setItem(item);
        viewHolder.bind(position);
    }
}
