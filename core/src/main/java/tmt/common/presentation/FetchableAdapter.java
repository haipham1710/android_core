package tmt.common.presentation;

import android.content.Context;

public interface FetchableAdapter {

    interface FetchingListener {
        void onBeginFetch();

        void onCompleteFetch();

        void hasException(Exception e);
    }

    void fetch();

    void notifyDataSetChanged();

    void setFetchingListener(FetchingListener listener);

    boolean isFetching();

    void reset();

    boolean refresh();

    void reset(String keyword);

    boolean isEmpty();

    void setContext(Context context);
}
