package tmt.common.presentation;

import android.view.View;

public class TypedViewHolder<E> extends ViewHolder {

    public TypedViewHolder(View view) {
        super(view);
    }

    @SuppressWarnings("unchecked")
    @Override
    public E item() {
        return (E) item;
    }
}
