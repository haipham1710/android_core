package tmt.common.presentation;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public abstract class BaseViewHolderAdapter<VH extends BaseViewHolder> extends BaseAdapter {

    public abstract int getCount();

    @Override
    public abstract Object getItem(int position);

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressWarnings("unchecked")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        VH view;
        if (convertView == null) {
            view = onCreateViewHolder(parent, getItemViewType(position));
        } else {
            view = (VH) convertView.getTag();
        }
        onBindViewHolder(view, position);
        return view.view;
    }

    public abstract VH onCreateViewHolder(ViewGroup parent, int viewType);

    public abstract void onBindViewHolder(VH viewHolder, int position);

}
