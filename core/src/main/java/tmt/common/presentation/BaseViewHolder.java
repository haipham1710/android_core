package tmt.common.presentation;

import android.view.View;

public abstract class BaseViewHolder {

    public final View view;

    public BaseViewHolder(View view) {
        this.view = view;
        view.setTag(this);
    }
}
