package tmt.common.presentation;

import android.content.Context;
import android.os.Parcelable;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import tmt.common.util.SavedInstanceStateData;

public abstract class SingeChoiceAdapter<T extends Parcelable> extends BaseListAdapter2<T> {

    public interface OnItemClickedListener<T> {
        void onItemClicked(T item);
    }

    private T selectedItem;
    private final
    @IdRes
    int radioButtonId;
    private RadioButton checkedRadioButton;
    private OnItemClickedListener<T> listener;

    public SingeChoiceAdapter(Context context, Iterable<T> items, @IdRes int radioButtonId) {
        super(context, items);
        this.radioButtonId = radioButtonId;
    }

    public SingeChoiceAdapter(Context context, T[] items, @IdRes int radioButtonId) {
        super(context, items);
        this.radioButtonId = radioButtonId;
    }

    public SingeChoiceAdapter(Context context, T empty, T[] items, @IdRes int radioButtonId) {
        super(context, empty, items);
        this.radioButtonId = radioButtonId;
    }

    public SingeChoiceAdapter(Context context, @IdRes int radioButtonId) {
        super(context);
        this.radioButtonId = radioButtonId;
    }

    @Override
    protected void readFromSavedState(SavedInstanceStateData source) {
        super.readFromSavedState(source);
        selectedItem = source.readObject();
    }

    @Override
    protected void writeState(SavedInstanceStateData dest) {
        super.writeState(dest);
        dest.writeObject(selectedItem);
    }

    @SuppressWarnings({ "unchecked" })
    @Override
    protected void onFirstBindViewHolder(final ViewHolder viewHolder, int position) {
        final RadioButton radioButton = (RadioButton) viewHolder.view.findViewById(radioButtonId);
        viewHolder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkedRadioButton != null) {
                    checkedRadioButton.setChecked(false);
                }
                checkedRadioButton = radioButton;
                checkedRadioButton.setChecked(true);
                selectedItem = (T) viewHolder.item();
                if (listener != null) {
                    listener.onItemClicked(selectedItem);
                }
            }
        });
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        super.onBindViewHolder(viewHolder, position);
        boolean isChecked = selectedItem != null && selectedItem.equals(getItem(position));
        RadioButton button = (RadioButton) viewHolder.view.findViewById(radioButtonId);
        button.setChecked(isChecked);
        if (isChecked) {
            if (checkedRadioButton == null) {
                checkedRadioButton = button;
            }
//            button.setChecked(true);
        }
    }

    @NonNull
    @Override
    public abstract TypedViewHolder<T> onCreateViewHolder(ViewGroup parent, int viewType);

    public T getSelectedItem() {
        return selectedItem;
    }

    public void setSelectedItem(T selectedItem) {
        this.selectedItem = selectedItem;
    }

    public void setOnItemClickedListener(OnItemClickedListener<T> listener) {
        this.listener = listener;
    }
}
