package tmt.common.presentation;

public interface Predicate<T> {
    boolean value(T t);
}
