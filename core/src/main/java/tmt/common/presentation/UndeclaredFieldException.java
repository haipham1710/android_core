package tmt.common.presentation;

public class UndeclaredFieldException extends RuntimeException {

    /**
     *
     */
    private static final long serialVersionUID = 9196005003820471232L;
    private Class<?> hostType;

    public Class<?> getHostType() {
        return hostType;
    }

    public String getFieldName() {
        return fieldName;
    }

    public Class<?> getValueType() {
        return valueType;
    }

    private String fieldName;
    private Class<?> valueType;

    public UndeclaredFieldException() {
        super();
    }

    public UndeclaredFieldException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public UndeclaredFieldException(String detailMessage) {
        super(detailMessage);
    }

    public UndeclaredFieldException(Throwable throwable) {
        super(throwable);
    }

    public UndeclaredFieldException(String fieldName, Class<?> valueType, Class<?> hostType) {
        super(generateDescription(fieldName, valueType, hostType));
        this.fieldName = fieldName;
        this.hostType = hostType;
        this.valueType = valueType;

    }

    private static String generateDescription(String fieldName, Class<?> valueType, Class<?> hostType) {
        return "Unknown field's named \"" + fieldName + "\" of type \"" + valueType.getName()
                + "\" in class \"" + hostType.getName() + ". You must declared this field";
    }
}
