package tmt.common.presentation;

import java.util.Comparator;

public interface Sortable<ClassType, FieldType extends Comparable<FieldType>> {
    FieldType field(ClassType e);

    final class Helper {

        public static <C, F extends Comparable<F>> Comparator<C> toComparator(final Sortable<C, F> sorter) {
            return new Comparator<C>() {

                @Override
                public int compare(C lhs, C rhs) {
                    if (lhs == rhs) {
                        return 0;
                    }
                    if (lhs == null) {
                        return -1;
                    }
                    if (rhs == null) {
                        return 1;
                    }
                    F left = sorter.field(lhs);
                    F right = sorter.field(rhs);
                    if (left == right) {
                        return 0;
                    }
                    if (left == null) {
                        return -1;
                    }
                    if (right == null) {
                        return 1;
                    }
                    return right.compareTo(left);
                }

            };

        }
    }
}
