package tmt.common;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class ImageHelper {

    public static String imageToBase64(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos); // bm is the
        // bitmap object
        byte[] byteArrayImage = baos.toByteArray();
        String encodedImage = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);
        try {
            baos.close();
        } catch (IOException ignored) {
        }
        return encodedImage;
    }

    public static Bitmap base64ToImage(String base64) {
        if (base64 == null || base64.length() == 0) {
            return null;
        }
        try {
            byte[] decodedString = Base64.decode(base64, Base64.NO_WRAP);
            return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Bitmap resizeBitmap(Bitmap bitmap, int max) {
        if (bitmap == null) {
            return null;
        }
        if (bitmap.getWidth() <= max && bitmap.getHeight() <= max) {
            return bitmap;
        }
        float ratio = (float) bitmap.getWidth() / (float) bitmap.getHeight();
        int width;
        int height;
        if (ratio > 1f) {
            // Width dimension is larger
            width = max;
            height = (int) (width / ratio);
        } else {
            height = max;
            width = (int) (height * ratio);
        }
        return Bitmap.createScaledBitmap(bitmap, width, height, false);
    }

}
