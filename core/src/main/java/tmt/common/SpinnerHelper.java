package tmt.common;

import android.os.Bundle;
import android.os.Parcelable;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

import java.util.ArrayList;

public class SpinnerHelper {
    public static class SpinnerItem<T extends Parcelable> {
        public ArrayList<T> items;
        public int selection;
    }

    public static <T extends Parcelable> void saveSpinnerState(Spinner spinner, Bundle savedBundleState, String key) {

        SpinnerAdapter adapter = spinner.getAdapter();
        if (adapter == null) {
            return;
        }
        String keyItems = key + "-items";
        String keySelection = key + "-selections";

        ArrayList<Parcelable> items = new ArrayList<>();
        for (int i = 0; i < adapter.getCount(); i++) {
            items.add((Parcelable) adapter.getItem(i));
        }

        int selection = spinner.getSelectedItemPosition();

        savedBundleState.putParcelableArrayList(keyItems, items);
        savedBundleState.putInt(keySelection, selection);
    }

    public static <T extends Parcelable> SpinnerItem<T> readSpinnerState(Bundle savedBundleState, String key) {
        if (savedBundleState == null) {
            return null;
        }
        String keyItems = key + "-items";
        String keySelection = key + "-selections";
        SpinnerItem<T> spinnerItem = new SpinnerItem<>();
        spinnerItem.items = savedBundleState.getParcelableArrayList(keyItems);
        spinnerItem.selection = savedBundleState.getInt(keySelection);
        return spinnerItem;
    }
}
