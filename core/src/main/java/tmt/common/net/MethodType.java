package tmt.common.net;

public enum MethodType {
    GET, POST, PUT, DELETE
}
