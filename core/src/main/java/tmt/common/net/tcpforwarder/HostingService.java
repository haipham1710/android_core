package tmt.common.net.tcpforwarder;

import android.os.AsyncTask;
import android.os.Handler;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.Socket;

import tmt.common.threading.Task;
import tmt.common.threading.TaskSource;

public abstract class HostingService implements Closeable {
    public enum Status {
        NotConnected,
        Verifying,
        Connecting,
        WaitingForReceiver,
        Authenticating,
        Connected
    }

    public interface OnStatusChanged {
        void onStatusChanged(Status status);
    }

    public OnStatusChanged StatusChanged;

    private final String _password;
    protected Socket _client;
    protected BufferedWriter _writer;
    protected BufferedReader _reader;
    private final String _sessionId;
    private Status _status = Status.NotConnected;
    private final Handler _invoker = new Handler();

    public String getSessionId() {
        return _sessionId;
    }

    public String getPassword() {
        return _password;
    }

    public HostingService(String sessionId, String password) {
        _sessionId = sessionId;
        _password = password;
    }

    public Task<Boolean> startAsync(final Status expectedStatus) {
        final TaskSource<Boolean> taskSource = new TaskSource<>();
        onStatusChanged(Status.NotConnected);
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                try {
                    onStatusChanged(HostingService.Status.Connecting);
                    completeTaskIfExpected(taskSource, expectedStatus);
                    if (!establishConnection()) {
                        taskSource.trySetResult(false);
                        return null;
                    }

                    onStatusChanged(HostingService.Status.Verifying);
                    completeTaskIfExpected(taskSource, expectedStatus);
                    if (!verifyConnection()) {
                        taskSource.trySetResult(false);
                        return null;
                    }

                    onStatusChanged(HostingService.Status.WaitingForReceiver);
                    completeTaskIfExpected(taskSource, expectedStatus);
                    if (!waitForReceiver()) {
                        taskSource.trySetResult(false);
                        return null;
                    }

                    onStatusChanged(HostingService.Status.Authenticating);
                    completeTaskIfExpected(taskSource, expectedStatus);
                    if (!authenticateReceiver()) {
                        taskSource.trySetResult(false);
                        return null;
                    }

                    onStatusChanged(HostingService.Status.Connected);
                    completeTaskIfExpected(taskSource, expectedStatus);
                    listen();
                } finally {
                    stopSession();
                }
                return null;
            }
        }.execute();
        return taskSource.getTask();
    }

    private void completeTaskIfExpected(TaskSource<Boolean> taskSource, Status expectedStatus) {
        if (_status == expectedStatus) {
            taskSource.setResult(true);
        }
    }

    protected void onStatusChanged(final Status status) {
        if (_status == status) {
            return;
        }
        _status = status;
        final OnStatusChanged handler = StatusChanged;
        if (handler != null) {
            _invoker.post(new Runnable() {

                @Override
                public void run() {
                    handler.onStatusChanged(status);
                }
            });
        }
    }

    @Override
    public void close() throws IOException {
        stopSession();
    }

    private boolean establishConnection() {
        _client = new Socket();

        try {
            _client.connect(new InetSocketAddress(DefaultTcpForwarderConfig.HOST, DefaultTcpForwarderConfig.PORT));
            _reader = new BufferedReader(new InputStreamReader(_client.getInputStream(), "UTF-8"));
            _writer = new BufferedWriter(new OutputStreamWriter(_client.getOutputStream(), "UTF-8"));
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    /// <summary>
    /// Write 1 + SessionId, Read HttpStatusCode
    /// </summary>
    private boolean verifyConnection() {
        String msg;
        try {
            _writer.write("1 " + _sessionId);
            _writer.newLine();
            _writer.flush();
            msg = _reader.readLine();
        } catch (Exception e) {
            return false;
        }
        return "200".equals(msg);
    }

    /// <summary>
    /// Read HttpStatusCode
    /// </summary>
    /// <returns></returns>
    private boolean waitForReceiver() {
        String msg;
        try {
            msg = _reader.readLine();
        } catch (Exception e) {
            return false;
        }
        return "100".equals(msg);
    }

    /// <summary>
    /// Read Password, Write HttpStatusCode
    /// </summary>
    /// <returns></returns>
    private boolean authenticateReceiver() {
        try {
            boolean isOK = _password.equals(_reader.readLine());
            _writer.write(isOK ? "200" : "401");
            _writer.newLine();
            _writer.flush();
            return isOK;
        } catch (IOException e) {
            return false;
        }
    }

    protected void stopSession() {
        // Close input stream before closing output stream makes socket stuck.
        // Don't change the order of closable object
        onStatusChanged(Status.NotConnected);
        closeDisposable(_writer);
        closeDisposable(_reader);
        closeDisposable(_client);
        _reader = null;
        _writer = null;
        _client = null;
    }

    protected void closeDisposable(Closeable obj) {
        if (obj != null) {
            try {
                obj.close();
            } catch (IOException ignored) {
            }
        }
    }

    protected abstract void listen();

}
