package tmt.common.net.tcpforwarder;

import android.os.AsyncTask;
import android.os.Handler;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.Socket;

import tmt.common.extension.CloseableExtension;
import tmt.common.threading.Task;
import tmt.common.threading.TaskSource;

public abstract class ClientService implements Closeable {
    public enum Status {
        Disconnected, Connecting, Verifying, Authenticating, Connected
    }

    public interface OnStatusChanged {
        void onStatusChanged(Status status);
    }

    private OnStatusChanged onStatusChanged;

    protected Socket _client;
    protected BufferedReader _reader;
    protected BufferedWriter _writer;

    private final String _password;
    private final String _sessionId;
    private final Handler _invoker = new Handler();
    private Status _status = Status.Disconnected;

    protected ClientService(String sessionId, String password) {
        _sessionId = sessionId;
        _password = password;
    }

    public void setStatusChangedListener(OnStatusChanged l) {
        onStatusChanged = l;
    }

    public Task<Boolean> startAsync(final Status expectedStatus) {
        _status = Status.Disconnected;
        final TaskSource<Boolean> taskSource = new TaskSource<>();
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {

                try {
                    onStatusChanged(ClientService.Status.Connecting);
                    completeTaskIfExpected(taskSource, expectedStatus);
                    if (!establishConnection()) {
                        taskSource.trySetResult(false);
                        return null;
                    }

                    onStatusChanged(ClientService.Status.Verifying);
                    completeTaskIfExpected(taskSource, expectedStatus);
                    if (!verifyConnection()) {
                        taskSource.trySetResult(false);
                        return null;
                    }

                    onStatusChanged(ClientService.Status.Authenticating);
                    completeTaskIfExpected(taskSource, expectedStatus);
                    if (!authenticateConnection()) {
                        taskSource.trySetResult(false);
                        return null;
                    }

                    onStatusChanged(ClientService.Status.Connected);
                    completeTaskIfExpected(taskSource, expectedStatus);
                    listen();
                } finally {
                    closeSession();
                }
                return null;
            }

        }.execute();
        return taskSource.getTask();
    }

    protected void closeSession() {
        onStatusChanged(Status.Disconnected);
        try {
            close();
        } catch (IOException ignored) {
        }
    }

    private void completeTaskIfExpected(TaskSource<Boolean> taskSource, Status expectedStatus) {
        if (_status == expectedStatus) {
            taskSource.setResult(true);
        }
    }

    protected void onStatusChanged(final Status status) {
        if (_status == status) {
            return;
        }
        _status = status;
        final OnStatusChanged handler = onStatusChanged;
        if (handler != null) {
            _invoker.post(new Runnable() {
                @Override
                public void run() {
                    handler.onStatusChanged(status);
                }
            });
        }
    }

    protected abstract void listen();

    private boolean authenticateConnection() {
        try {
            _writer.write(_password);
            _writer.newLine();
            _writer.flush();
            String ans = _reader.readLine();
            return "200".equals(ans);
        } catch (Exception e) {
            return false;
        }
    }

    private boolean establishConnection() {
        _client = new Socket();

        try {
            _client.connect(new InetSocketAddress(DefaultTcpForwarderConfig.HOST, DefaultTcpForwarderConfig.PORT));
            _reader = new BufferedReader(new InputStreamReader(_client.getInputStream(), "UTF-8"));
            _writer = new BufferedWriter(new OutputStreamWriter(_client.getOutputStream(), "UTF-8"));
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public boolean verifyConnection() {
        try {
            _writer.write("2 " + _sessionId);
            _writer.newLine();
            _writer.flush();
            String ans = _reader.readLine();
            return "200".equals(ans);
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public void close() throws IOException {
        CloseableExtension.forceClose(_reader);
        CloseableExtension.forceClose(_writer);
        CloseableExtension.forceClose(_client);
        _reader = null;
        _writer = null;
        _client = null;
        _status = Status.Disconnected;
    }
}
