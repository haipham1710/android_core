package tmt.common.net;

public class HttpStatusException extends Exception {
    public final int statusCode;
    public final String content;

    public HttpStatusException(String content, int statusCode) {
        super(String.format("%d %s: %s", statusCode, HttpStatus.getStatusText(statusCode), content));
        this.statusCode = statusCode;
        this.content = content;
    }

    /**
     *
     */
    private static final long serialVersionUID = -4314839439005366241L;

    public final boolean isOk() {
        return statusCode == HttpStatus.SC_OK;
    }

}
