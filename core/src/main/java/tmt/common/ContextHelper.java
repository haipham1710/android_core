package tmt.common;

import android.app.Activity;
import android.util.DisplayMetrics;
import android.view.Surface;

import tmt.common.presentation.ScreenOrientation;

public class ContextHelper {
    private ContextHelper() {
    }

    public static ScreenOrientation getScreenOrientation(Activity context) {
        int rotation = context.getWindowManager().getDefaultDisplay().getRotation();
        DisplayMetrics dm = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;
        int height = dm.heightPixels;
        // if the device's natural orientation is portrait:
        if ((rotation == Surface.ROTATION_0
                || rotation == Surface.ROTATION_180) && height > width ||
                (rotation == Surface.ROTATION_90
                        || rotation == Surface.ROTATION_270) && width > height) {
            switch (rotation) {
                case Surface.ROTATION_0:
                    return ScreenOrientation.Portrait;
                case Surface.ROTATION_90:
                    return ScreenOrientation.Landscape;
                case Surface.ROTATION_180:
                    return ScreenOrientation.FlippedPortrait;
                case Surface.ROTATION_270:
                    return ScreenOrientation.FlippedLandscape;
                default:
                    return ScreenOrientation.Portrait;
            }
        }
        // if the device's natural orientation is landscape or if the device
        // is square:
        else {
            switch (rotation) {
                case Surface.ROTATION_0:
                    return ScreenOrientation.Landscape;
                case Surface.ROTATION_90:
                    return ScreenOrientation.Portrait;
                case Surface.ROTATION_180:
                    return ScreenOrientation.FlippedLandscape;
                case Surface.ROTATION_270:
                    return ScreenOrientation.FlippedPortrait;
                default:
                    return ScreenOrientation.Landscape;
            }
        }

    }
}
