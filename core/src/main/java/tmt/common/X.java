package tmt.common;

/**
 * Mystics class
 *
 * @author Starfall
 */
public class X {

    private X() {
    }

    public static <T> T coalesce(T one, T two) {
        return one != null ? one : two;
    }

    public static <T> T coalesce(T one, T two, T three) {
        return one != null ? one : (two != null ? two : three);
    }

    public static <T> T coalesce(T one, T two, T three, T four) {
        return one != null ? one : (two != null ? two : (three != null ? three : four));
    }

    public static <T> T coalesce(T one, T two, T three, T four, T five) {
        return one != null ? one : (two != null ? two : (three != null ? three : (four != null ? four : five)));
    }
}
