package tmt.common;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

public class Duration implements Parcelable {

    public static final Duration ZERO = new Duration(0);

    public static final int FIELD_MILLIS_3_DIGIT = 0;
    public static final int FIELD_SECOND = 1;
    public static final int FIELD_MINUTE = 2;
    public static final int FIELD_HOUR = 3;
    public static final int FIELD_DAY = 4;

    public final long milliseconds;

    private final int[] fields = new int[5];
    private static final String[] msg = { "ss ", "s ", "m ", "h ", "d " };

    public Duration(long milliseconds) {
        this.milliseconds = milliseconds;
        fields[FIELD_MILLIS_3_DIGIT] = (int) (milliseconds % 1000);
        milliseconds = milliseconds / 1000;
        fields[FIELD_SECOND] = (int) (milliseconds % 60);
        milliseconds = milliseconds / 60;
        fields[FIELD_MINUTE] = (int) (milliseconds % 60);
        milliseconds = milliseconds / 60;
        fields[FIELD_HOUR] = (int) (milliseconds % 24);
        fields[FIELD_DAY] = (int) (milliseconds / 24);
    }

    public Duration(Date fromDate, Date toDate) {
        this(toDate.getTime() - fromDate.getTime());
    }

    public long getMilliseconds() {
        return milliseconds;
    }

    public long getSeconds() {
        return fields[FIELD_SECOND];
    }

    public int getMinutes() {
        return fields[FIELD_MINUTE];
    }

    public int getHours() {
        return fields[FIELD_HOUR];
    }

    public int getDays() {
        return fields[FIELD_DAY];
    }

    @Override
    public String toString() {
        return txString(FIELD_MILLIS_3_DIGIT, 0);
    }

    public String txString(int detailLevel, int maxElement) {
        StringBuilder sb = new StringBuilder();
        int y = 0;
        for (int i = fields.length - 1; i > 0; i--) {
            if (i < detailLevel && y >= maxElement) {
                break;
            }
            if (sb.length() == 0 && fields[i] == 0) {
                continue;
            }
            y++;
            sb.append(fields[i]).append(msg[i]);
        }
        return sb.toString();
    }



    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(milliseconds);
    }

    public static final Creator<Duration> CREATOR = new Creator<Duration>() {

        @Override
        public Duration[] newArray(int size) {
            return new Duration[size];
        }

        @Override
        public Duration createFromParcel(Parcel source) {
            return new Duration(source.readLong());
        }
    };
}
