package tmt.common.threading;

import android.os.AsyncTask;

import java.util.concurrent.CancellationException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public abstract class TaskFactory<V> implements ITaskFactory<V> {

    private final AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {

        @Override
        protected Void doInBackground(Void... params) {
            V value;
            try {
                value = run();
            } catch (CancellationException e) {
                source.setCancellation();
                return null;
            } catch (Exception e) {
                source.setException(e);
                return null;
            }
            source.setResult(value);
            return null;
        }

    };
    private final TaskSource<V> source = new TaskSource<>();

    protected abstract V run() throws Exception;

    private boolean ran;

    @Override
    public Task<V> startNew() {
        if (!ran) {
            ran = true;
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
        return source.getTask();
    }

    public static <V> TaskFactory<V> emptyTask(final V value) {
        return new TaskFactory<V>() {

            @Override
            protected V run() throws Exception {
                throw new IllegalStateException("Can not run from empty task");
            }

            @Override
            public Task<V> startNew() {
                return TaskSource.emptyTask(value);
            }
        };
    }

    public static <V> TaskFactory<V> fromFuture(final Future<V> task, final long milliseconds) {
        return new TaskFactory<V>() {
            @Override
            protected V run() throws Exception {
                try {
                    return task.get(milliseconds, TimeUnit.MILLISECONDS);
                } catch (TimeoutException ignored) {
                    throw new CancellationException();
                }
            }
        };
    }
}
