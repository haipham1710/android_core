package tmt.common.threading;

import android.os.Handler;
import android.os.Looper;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public abstract class TaskScheduler {

    private static Handler s_handler = null;
    private static final ThreadPoolTaskScheduler s_background = new ThreadPoolTaskScheduler("Background TaskScheduler");

    /**
     * For debugging
     */
    private final String name;


    protected TaskScheduler(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    /**
     * The {@code TaskScheduler} that performs task in main thread
     *
     * @return TaskScheduler
     */
    public static TaskScheduler synchronizedContext() {
        return new HandlerTaskScheduler("Synchronized TaskScheduler", synchronizedHandler());
    }

    private synchronized static Handler synchronizedHandler() {
        if (s_handler == null) {
            s_handler = new Handler(Looper.getMainLooper());
        }
        return s_handler;
    }

    public static TaskScheduler fromHandler(Handler handler) {
        return new HandlerTaskScheduler("Unknown Handler TaskScheduler", handler);
    }


    /**
     * The {@code TaskScheduler} that performs task in main thread pool
     *
     * @return TaskScheduler
     */
    public static TaskScheduler backgroundContext() {
        return s_background;
    }

    public abstract void post(Runnable action);
}

class HandlerTaskScheduler extends TaskScheduler {
    private final Handler handler;

    HandlerTaskScheduler(String name, Handler handler) {
        super(name);
        this.handler = handler;
    }

    public void post(Runnable action) {
        handler.post(action);
    }
}

class ThreadPoolTaskScheduler extends TaskScheduler {
    private final ScheduledExecutorService executor = Executors.newScheduledThreadPool(6);

    ThreadPoolTaskScheduler(String name) {
        super(name);
    }

    @Override
    public void post(final Runnable action) {
        executor.execute(action);
    }
}
