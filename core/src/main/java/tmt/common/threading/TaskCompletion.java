package tmt.common.threading;

public interface TaskCompletion<V> {
    void onContinue(Task<V> task) throws Exception;
}
