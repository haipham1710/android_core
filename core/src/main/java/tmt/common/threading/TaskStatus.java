package tmt.common.threading;

public enum TaskStatus {
    Ready, Running, Completed, Cancelled, Faulted,
}
