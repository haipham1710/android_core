package tmt.common.threading;

import java.util.concurrent.CancellationException;

public class TaskExtensions {

    public static Exception getException(Task<?> task) {
        if (task.isCancelled()) {
            return new CancellationException();
        }
        if (task.isFaulted()) {
            Throwable e = task.getException();
            while (e instanceof TaskIsFaultedException && e.getCause() != null) {
                e = e.getCause();
            }
            return (Exception) e;
        }
        return null;
    }
}
