package tmt.common.threading;

public interface ITaskFactory<V> {
    /**
     * return the {@code Task}} that {@code ITaskFactory} holds.
     * Start this task if it hasn't ran yet.
     *
     */
    Task<V> startNew();
}
