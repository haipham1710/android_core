package tmt.common.threading;

public class TaskSource<V> {

    private final TaskData<V> data = new TaskData<>();
    private final Task<V> task = new Task<>(data);

    public TaskSource() {
    }

    public TaskSource(TaskScheduler currentScheduler) {
        data.currentScheduler = currentScheduler;
    }

    public void setResult(V value) {
        if (data.status == TaskStatus.Ready || data.status == TaskStatus.Running) {
            data.status = TaskStatus.Completed;
            data.result = value;
            data.notifyCompletion(task);
        } else {
            throw new IllegalStateException("Task is not running");
        }
    }

    public void trySetResult(V value) {
        if (data.status == TaskStatus.Ready || data.status == TaskStatus.Running) {
            data.status = TaskStatus.Completed;
            data.result = value;
            data.notifyCompletion(task);
        }
    }

    public void setException(Exception exception) {
        if (!trySetException(exception)) {
            throw new IllegalStateException("Task is not running");
        }
    }

    public boolean trySetException(Exception exception) {
        if (data.status == TaskStatus.Ready || data.status == TaskStatus.Running) {
            data.status = TaskStatus.Faulted;
            data.exception = exception;
            data.notifyCompletion(task);
            return true;
        }
        return false;

    }

    public void setCancellation() {
        if (!trySetCancellation()) {
            throw new IllegalStateException("Task is not running");
        }
    }

    public boolean trySetCancellation() {
        if (data.status == TaskStatus.Ready || data.status == TaskStatus.Running) {
            data.status = TaskStatus.Cancelled;
            data.notifyCompletion(task);
            return true;
        }
        return false;
    }

    public Task<V> getTask() {
        return task;
    }

    public static <V> Task<V> emptyTask(V result) {
        TaskSource<V> source = new TaskSource<>();
        source.setResult(result);
        return source.getTask();
    }

    public static <V> Task<V> cancelledTask() {
        TaskSource<V> source = new TaskSource<>();
        source.setCancellation();
        return source.getTask();
    }
}
