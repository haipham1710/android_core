package tmt.common.datacontract;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateParser {
    private static final SimpleDateFormat ISO08601 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);

    public static Date parse(String input) throws java.text.ParseException {
        if (input == null || input.length() == 0) {
            return null;
        }
        return ISO08601.parse(input);

    }

    public static String format(Date date) {
        if (date == null) {
            return null;
        }
        return ISO08601.format(date);
    }

}
