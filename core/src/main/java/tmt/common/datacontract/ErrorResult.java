package tmt.common.datacontract;

import android.os.Parcel;
import android.os.Parcelable;

import java.net.UnknownHostException;

import tmt.common.net.HttpStatus;
import tmt.common.net.HttpStatusException;
import tmt.common.presentation.Activator;
import tmt.common.threading.Task;
import tmt.common.util.BaseCreator;

public class ErrorResult implements Parcelable {

    public static final int EXCEPTION = -10;
    public static final String KeyId = "Id";
    public static final String KeyMessage = "Message";
    public static final String KeySuccess = "Success";

    public static ErrorResult makeError(Exception e) {
        return makeError(e.getMessage());
    }

    public static ErrorResult makeError(String message) {
        return new ErrorResult(true, message);
    }

    public static ErrorResult makeSuccess(String message) {
        return new ErrorResult(false, message);
    }

    public static <T extends ErrorResult> ErrorResult wrapFinishTask(Task<T> task) {
        if (task.isCompleted()) {
            return task.getResult();
        }
        if (task.isCancelled()) {
            return ErrorResult.makeError("Thao tác bị hủy");
        }
        Exception e = task.getException();
        if (e instanceof UnknownHostException) {
            return ErrorResult.makeError("Không kết nối được máy chủ");
        } else if (e instanceof HttpStatusException) {
            return ErrorResult.makeError(HttpStatus.getStatusText(((HttpStatusException) e).statusCode));
        }
        return ErrorResult.makeError(e);
    }

    public static ErrorResult wrapVoidTask(Task<Void> task) {
        if (task.isCompleted()) {
            return ErrorResult.makeSuccess("Success");
        }
        if (task.isCancelled()) {
            return ErrorResult.makeError("Thao tác bị hủy");
        }
        Exception e = task.getException();
        if (e instanceof UnknownHostException) {
            return ErrorResult.makeError("Không kết nối được máy chủ");
        } else if (e instanceof HttpStatusException) {
            return ErrorResult.makeError(HttpStatus.getStatusText(((HttpStatusException) e).statusCode));
        }
        return ErrorResult.makeError(e);
    }


    private String id;

    private boolean isError;

    private String message;

    public ErrorResult() {
    }

    public ErrorResult(JsonHelper helper) {
        isError = !helper.readBoolean(KeySuccess, false);
        message = helper.readString(KeyMessage, "Lỗi khi đọc dữ liệu từ máy chủ");
        id = helper.readString(KeyId);
    }

    public ErrorResult(boolean isError, String message) {
        this.isError = isError;
        this.message = message;
    }

    protected ErrorResult(Parcel source) {
        isError = source.readInt() != 0;
        message = source.readString();
        id = source.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String entityId() {
        return id;
    }

    public boolean isError() {
        return isError;
    }

    public final String message() {
        return message;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(isError ? 1 : 0);
        dest.writeString(message);
        dest.writeString(id);
    }

    public final static Activator<ErrorResult> ACTIVATOR = new Activator<ErrorResult>() {

        @Override
        public ErrorResult createFromException(Exception e) {
            return ErrorResult.makeError(e);
        }

        @Override
        public ErrorResult[] newArray(int size) {
            return new ErrorResult[size];
        }

        @Override
        public ErrorResult newInstance() {
            return new ErrorResult();
        }
    };

    public static final BaseCreator<ErrorResult> CREATOR = new BaseCreator<ErrorResult>() {

        public ErrorResult createFromJson(JsonHelper helper) {
            return new ErrorResult(helper);
        }

        public ErrorResult createFromParcel(Parcel source) {
            return new ErrorResult(source);
        }

        @Override
        public ErrorResult[] newArray(int size) {
            return new ErrorResult[size];
        }

    };
}
