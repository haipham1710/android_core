package tmt.common.view.utils;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;

public class HackView {
    private HackView() {
    }

    public static Fragment getCurrentFragmentInViewPager(FragmentManager fm, ViewPager pager) {
        if (pager.getCurrentItem() < 0) {
            return null;
        }
        return fm.findFragmentByTag("android:switcher:" + pager.getId() + ":" + pager.getCurrentItem());
    }
}
