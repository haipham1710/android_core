package tmt.common.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ScrollView;

public class ListenableScrollView extends ScrollView {

    public interface OnScrollChangedListener {
        void onScrollChanged(int l, int t, int oldl, int oldt);

        void onScrollIdle();
    }

    private OnScrollChangedListener onScrollChangedListener;
    private boolean mScrollChangedSinceLastIdle;

    public ListenableScrollView(Context context) {
        super(context);
    }

    public ListenableScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ListenableScrollView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);
        if (onScrollChangedListener != null) {
            onScrollChangedListener.onScrollChanged(l, t, oldl, oldt);
        }
    }

    public void setOnScrollChangedListener(OnScrollChangedListener listener) {
        this.onScrollChangedListener = listener;
    }
    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        boolean b = super.onTouchEvent(ev);
        if (ev.getAction() == MotionEvent.ACTION_UP) {
            if (onScrollChangedListener != null && mScrollChangedSinceLastIdle) {
                mScrollChangedSinceLastIdle = false;
                onScrollChangedListener.onScrollIdle();
            }
        }
        return b;
    }
}
