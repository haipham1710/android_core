package tmt.common.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.ImageView;

public class AspectRatioImageView extends ImageView {
    private double ratio;
    private boolean fixedByHorizontal;

    public AspectRatioImageView(Context context) {
        super(context);
    }

    public AspectRatioImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AspectRatioImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.AspectRatioImageView,
                0, 0
                );
        try {
            fixedByHorizontal = a.getInteger(R.styleable.AspectRatioImageView_fixedOrientation, 0) == 0;
            ratio = a.getFloat(R.styleable.AspectRatioImageView_ratio, 0);
        } finally {
            a.recycle();
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (ratio == 0) {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
            return;
        }
        int width, height;
        if (fixedByHorizontal) {
            width = MeasureSpec.getSize(widthMeasureSpec);
            height = (int) (width / ratio);
            setMeasuredDimension(width, height);
        } else {
            height = MeasureSpec.getSize(heightMeasureSpec);
            width = (int) (height * ratio);
        }
        setMeasuredDimension(width, height);
    }

    /**
     * Get the ratio of width divided by height (ratio = width / height)
     * 
     * @return the value of width divided by height
     */
    public double getRatio() {
        return ratio;
    }

    /**
     * Set the ratio of width divided by height (ratio = width / height)
     * @param ratio
     */
    public void setRatio(double ratio) {
        this.ratio = ratio;
    }
}
