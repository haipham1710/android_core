package tmt.common.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ScrollView;

/**
 * Custom ScrollView with header always on top
 * 
 * @author Starfall
 *
 */
public class SynchronizedScrollView extends ScrollView {
    public interface OnScrollChangedListener {
        void onScrollChanged(int l, int t, int oldl, int oldt);
    }

    private View mAnchorView;
    private View mSyncView;
    private int anchorViewId;
    private int synchronizedViewId;
    private int topMarginAnchor;
    private OnScrollChangedListener onScrollChangedListener;

    public SynchronizedScrollView(Context context) {
        super(context);
    }

    public SynchronizedScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
        onAttributeSet(context, attrs);
    }

    public SynchronizedScrollView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        onAttributeSet(context, attrs);
    }

    private void onAttributeSet(Context context, AttributeSet attrs) {
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.SynchronizedScrollView,
                0, 0);

        try {
            anchorViewId = a.getResourceId(R.styleable.SynchronizedScrollView_anchorView, 0);
            synchronizedViewId = a.getResourceId(R.styleable.SynchronizedScrollView_synchronizedView, 0);
            topMarginAnchor = a.getDimensionPixelSize(R.styleable.SynchronizedScrollView_topMarginAchor, 0);
        } finally {
            a.recycle();
        }
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        if (anchorViewId != 0) {
            mAnchorView = findViewById(anchorViewId);
        }
        if (synchronizedViewId != 0) {
            mSyncView = findViewById(synchronizedViewId);
        }
    }

    /**
     * Attach the appropriate child view to monitor during scrolling
     * as the anchoring space for the floating view. This view MUST
     * be an existing child.
     * 
     * @param v
     *            View to manage as the anchoring space
     */
    public void setAnchorView(View v) {
        mAnchorView = v;
        syncViews();
    }

    /**
     * Attach the appropriate child view to managed during scrolling
     * as the floating view. This view MUST be an existing child.
     * 
     * @param v
     *            View to manage as the floating view
     */
    public void setSynchronizedView(View v) {
        mSyncView = v;
        syncViews();
    }

    public int getTopMarginAnchor() {
        return topMarginAnchor;
    }

    public void setTopMarginAnchor(int topMarginAnchor) {
        this.topMarginAnchor = topMarginAnchor;
    }

    // Position the views together
    private void syncViews() {
        if (mAnchorView == null || mSyncView == null) {
            return;
        }

        // Distance between the anchor view and the header view
        int distance = mAnchorView.getTop() - mSyncView.getTop();
        mSyncView.offsetTopAndBottom(distance);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        // Calling this here attaches the views together if they were added
        // before layout finished
        syncViews();
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);
        if (mAnchorView == null || mSyncView == null) {
            return;
        }

        // Distance between the anchor view and the scroll position
        int matchDistance = mAnchorView.getTop() - getScrollY();
        // Distance between scroll position and sync view
        // Check if anchor is scrolled off screen
        if (matchDistance < topMarginAnchor) {
            int offset = getScrollY() - mSyncView.getTop() + topMarginAnchor;
            mSyncView.offsetTopAndBottom(offset);
        } else {
            syncViews();
        }
        if (onScrollChangedListener != null) {
            onScrollChangedListener.onScrollChanged(l, t, oldl, oldt);
        }
    }

    public void setOnScrollChangedListener(OnScrollChangedListener listener) {
        this.onScrollChangedListener = listener;
    }

}