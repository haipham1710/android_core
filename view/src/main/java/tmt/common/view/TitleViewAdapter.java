package tmt.common.view;

import android.view.View;
import android.view.ViewGroup;

public interface TitleViewAdapter {
    View getTitleViewAt(int position, ViewGroup parent);
    int getCount();
}
