package tmt.common.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.CheckedTextView;

public class CheckableTextView extends CheckedTextView {
    public interface OnCheckedChangeListener {
        void onCheckedChange(CheckableTextView textView, boolean checked);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CheckableTextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public CheckableTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public CheckableTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CheckableTextView(Context context) {
        super(context);
    }

    private OnCheckedChangeListener onCheckedChangeListener;

    @Override
    public void setChecked(boolean checked) {
        boolean shouldNotify = isChecked() != checked;
        super.setChecked(checked);
        if (shouldNotify && onCheckedChangeListener != null) {
            onCheckedChangeListener.onCheckedChange(this, checked);
        }
    }

    public void setOnCheckedChangeListener(OnCheckedChangeListener l) {
        this.onCheckedChangeListener = l;
    }
}
